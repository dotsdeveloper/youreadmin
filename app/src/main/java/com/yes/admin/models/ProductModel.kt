package com.yes.admin.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class ProductModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("user_id")
    var user_id: Int? = 0

    @JsonProperty("category_id")
    var category_id: Int? = 0

    @JsonProperty("sub_category_id")
    var sub_category_id: Int? = 0

    @JsonProperty("unit_id")
    var unit_id: Int? = 0

    @JsonProperty("file")
    var file: String? = ""

    @JsonProperty("name")
    var name: String? = ""

    @JsonProperty("brand_id")
    var brand_id: Int?= 0

    @JsonProperty("description")
    var description: String? = ""

    @JsonProperty("quantity")
    var quantity: String? = ""

    @JsonProperty("amount")
    var amount: String? = ""

    @JsonProperty("offer_price")
    var offer_price: String? = ""

    @JsonProperty("home_display")
    var home_display: Int? = 0

    @JsonProperty("enquiry")
    var enquiry: Int? = 0

    @JsonProperty("stock_available")
    var stock_available: Int? = 0

    @JsonProperty("display")
    var display: Int? = 0

    @JsonProperty("hotsale")
    var hotsale: Int? = 0

    @JsonProperty("available_from_time")
    var available_from_time: String? = ""

    @JsonProperty("available_to_time")
    var available_to_time: String? = ""

    @JsonProperty("is_recommend")
    var is_recommend: Int? = 0

    @JsonProperty("low_stock")
    var low_stock: Int? = 0

    @JsonProperty("ordering")
    var ordering: Int? = 0

    @JsonProperty("status")
    var status: Int? = 0

    @JsonProperty("draft")
    var draft: Int? = 0

    @JsonProperty("created_at")
    var created_at: String? = ""

    @JsonProperty("updated_at")
    var updated_at: String? = ""

    @JsonProperty("original_files")
    var original_files: Array<String>? = arrayOf(String())

    @JsonProperty("savedpercentage")
    var savedpercentage: String? = ""

    // @JsonAdapter(value = FilessJsonAdapter::class)
    /* @JsonProperty("files")
     var files: Array<String>? = arrayOf(String())*/

    @JsonProperty("filesNew")
    val filesNew: Files = Files()

    @JsonProperty("files")
    val files: Any = Files()

    @JsonProperty("quantities")
    var quantities: ArrayList<QuantitiesModel>? = ArrayList()

    @JsonProperty("category")
    var category: CategoryListModel? = CategoryListModel()

    @JsonProperty("subcategory")
    var subcategory: SubCategoryListModel? = SubCategoryListModel()

    @JsonProperty("brand")
    var brand: BrandModel? = BrandModel()
}

class Files : ArrayList<String>()
