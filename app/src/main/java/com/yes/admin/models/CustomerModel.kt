package com.yes.admin.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class CustomerModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("customer_id")
    var customer_id: Int? = 0

    @JsonProperty("name")
    var name: String? = ""

    @JsonProperty("parent_id")
    var parent_id: Int? = 0

    @JsonProperty("shopper_id")
    var shopper_id: Int? = 0

    @JsonProperty("channel_partner_id")
    var channel_partner_id: String? = ""

    @JsonProperty("channel_partner_lead_id")
    var channel_partner_lead_id: String? = ""

    @JsonProperty("email")
    var email: String? = ""

    @JsonProperty("mobile")
    var mobile: String? = ""

    @JsonProperty("username")
    var username: String? = ""

    @JsonProperty("email_verified_at")
    var email_verified_at: String? = ""

    @JsonProperty("user_type")
    var user_type: String? = ""

    @JsonProperty("role_id")
    var role_id: String? = ""

    @JsonProperty("customer_type")
    var customer_type: String? = ""

    @JsonProperty("location")
    var location: String? = ""

    @JsonProperty("domain")
    var domain: String? = ""

    @JsonProperty("language")
    var language: String? = ""

    @JsonProperty("country_code")
    var country_code: String? = ""

    @JsonProperty("avatar")
    var avatar: String? = ""

    @JsonProperty("is_first")
    var is_first: String? = ""

    @JsonProperty("is_verified")
    var is_verified: String? = ""

    @JsonProperty("is_on")
    var is_on: String? = ""

    @JsonProperty("is_master")
    var is_master: String? = ""

    @JsonProperty("status")
    var status: Int? = 0

    @JsonProperty("referral_userid")
    var referral_userid: String? = ""

    @JsonProperty("referral_code")
    var referral_code: String? = ""

    @JsonProperty("android_device_id")
    var android_device_id: String? = ""

    @JsonProperty("draft")
    var draft: String? = ""

    @JsonProperty("created_at")
    var created_at: String? = ""

    @JsonProperty("updated_at")
    var updated_at: String? = ""

    @JsonProperty("ref_count")
    var ref_count: String? = ""

    @JsonProperty("is_credit")
    var is_credit: String? = ""

    @JsonProperty("balance")
    var balance: String? = ""

    @JsonProperty("total_reward_avail")
    var total_reward_avail: String? = ""

    @JsonProperty("order_history")
    var order_history: OrderHistory? = OrderHistory()

    @JsonProperty("cancel_order_history")
    var cancel_order_history: CancelOrderHistory? = CancelOrderHistory()

    @JsonProperty("user")
    var user: User? = User()

    @JsonProperty("ref_user")
    var ref_user: ArrayList<RefUser>? = ArrayList()

    @JsonProperty("credit")
    var credit: Credit? = Credit()

    @JsonProperty("reward_point")
    var reward_point: ArrayList<RewardPoint>? = ArrayList()

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    class Credit : Serializable{
        @JsonProperty("id")
        var id: Int? = null

        @JsonProperty("customer_id")
        var customer_id: Int? = null

        @JsonProperty("shopper_id")
        var shopper_id: Int? = null

        @JsonProperty("credit_amount")
        var credit_amount: String? = null

        @JsonProperty("used_amount")
        var used_amount: String? = null

        @JsonProperty("description")
        var description: String? = null

        @JsonProperty("status")
        var status: String? = null
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    class RefUser : Serializable{
        @JsonProperty("id")
        var id: Int? = 0

        @JsonProperty("customer_id")
        var customer_id: Int? = 0

        @JsonProperty("shopper_id")
        var shopper_id: Int? = 0

        @JsonProperty("customer_type")
        var customer_type: String? = ""

        @JsonProperty("status")
        var status: String? = ""

        @JsonProperty("referral_userid")
        var referral_userid: String? = ""

        @JsonProperty("referral_code")
        var referral_code: String? = ""

        @JsonProperty("android_device_id")
        var android_device_id: String? = ""

        @JsonProperty("draft")
        var draft: String? = ""

        @JsonProperty("created_at")
        var created_at: String? = ""

        @JsonProperty("updated_at")
        var updated_at: String? = ""

        @JsonProperty("user")
        var user: User? = User()
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    class RewardPoint : Serializable{

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    class OrderHistory :Serializable {
        @JsonProperty("customer_id")
        var customer_id: Int? = 0

        @JsonProperty("user_count")
        var user_count: Int? = 0

        @JsonProperty("order_total")
        var order_total: String? = ""
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    class CancelOrderHistory : Serializable{

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    class User : Serializable {
        @JsonProperty("id")
        var id: Int? = null

        @JsonProperty("name")
        var name: String? = null

        @JsonProperty("parent_id")
        var parent_id: Int? = null

        @JsonProperty("shopper_id")
        var shopper_id: Int? = null

        @JsonProperty("channel_partner_id")
        var channel_partner_id: String? = null

        @JsonProperty("channel_partner_lead_id")
        var channel_partner_lead_id: String? = null

        @JsonProperty("email")
        var email: String? = null

        @JsonProperty("mobile")
        var mobile: String? = null

        @JsonProperty("username")
        var username: String? = null

        @JsonProperty("email_verified_at")
        var email_verified_at: String? = null

        @JsonProperty("user_type")
        var user_type: String? = null

        @JsonProperty("role_id")
        var role_id: String? = null

        @JsonProperty("customer_type")
        var customer_type: String? = null

        @JsonProperty("location")
        var location: String? = null

        @JsonProperty("domain")
        var domain: String? = null

        @JsonProperty("language")
        var language: String? = null

        @JsonProperty("country_code")
        var country_code: String? = null

        @JsonProperty("avatar")
        var avatar: String? = null

        @JsonProperty("is_first")
        var is_first: String? = null

        @JsonProperty("is_verified")
        var is_verified: String? = null

        @JsonProperty("is_on")
        var is_on: String? = null

        @JsonProperty("status")
        var status: String? = null

        @JsonProperty("is_master")
        var is_master: String? = null
    }
}


