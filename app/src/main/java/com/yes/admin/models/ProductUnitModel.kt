package com.yes.admin.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class ProductUnitModel : Serializable {
    @JsonProperty("id")
    var id: Int? = 0

    @JsonProperty("user_id")
    var user_id: Int? = 0

    @JsonProperty("name")
    var name: String? = ""

    @JsonProperty("status")
    var status: Int? = 0

    @JsonProperty("draft")
    var draft: Int? = 0

    @JsonProperty("created_by")
    var created_by: String? = ""

    @JsonProperty("updated_by")
    var updated_by: String? = ""

    @JsonProperty("created_at")
    var created_at: String? = ""

    @JsonProperty("updated_at")
    var updated_at: String? = ""
}
