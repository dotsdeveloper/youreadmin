package com.yes.admin.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
class AllCountModel : Serializable {
    @JsonProperty("total_order")
    var total_order: Int? = 0

    @JsonProperty("pending_order")
    var pending_order: Int? = 0

    @JsonProperty("completed_order")
    var completed_order: Int? = 0

    @JsonProperty("cancel_order")
    var cancel_order: Int? = 0

    @JsonProperty("current_month_order")
    var current_month_order: Int? = 0

    @JsonProperty("order_value_last_6_month")
    var order_value_last_6_month: Int? = 0

    @JsonProperty("order_last_7")
    var order_last_7: Int? = 0

    @JsonProperty("is_customer_order")
    var is_customer_order: Int? = 0

    @JsonProperty("is_direct_order")
    var is_direct_order: Int? = 0

    @JsonProperty("category_count")
    var category_count: Int? = 0

    @JsonProperty("product_count")
    var product_count: Int? = 0

    @JsonProperty("sub_category_count")
    var sub_category_count: Int? = 0

    @JsonProperty("customer_count")
    var customer_count: Int? = 0

    @JsonProperty("order_count")
    var order_count: Int? = 0

    @JsonProperty("sms_limit")
    var sms_limit: Int? = 0

    @JsonProperty("sms_count")
    var sms_count: Int? = 0

    /*@JsonProperty("order_value_last_30")
    var order_value_last_30: Int? = 0

    @JsonProperty("order_avg_value")
    var order_avg_value: Int? = 0*/

    @JsonProperty("order_last_30")
    var order_last_30: ArrayList<Any>? = ArrayList()


}
