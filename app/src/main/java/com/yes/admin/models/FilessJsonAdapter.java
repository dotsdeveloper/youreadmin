package com.yes.admin.models;

import android.util.Log;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;

public class FilessJsonAdapter extends TypeAdapter<Files> {

    @Override
    public void write(JsonWriter out, Files user) throws IOException {

        // Since we do not serialize EquationList by gson we can omit this part.
        // If you need you can check
        // com.google.gson.internal.bind.ObjectTypeAdapter class
        // read method for a basic object serialize implementation
        Log.d("nbvc","bhjhjb"+user.toString());

    }

    @Override
    public Files read(JsonReader in) throws IOException {

        Files deserializedObject = new Files();

        // type of next token
        JsonToken peek = in.peek();

        Log.d("nbvc",""+JsonToken.STRING.equals(peek));
        // if the json field is string
        if (JsonToken.STRING.equals(peek)) {
            String stringValue = in.nextString();
            // convert string to integer and add to list as a value
            deserializedObject.add(stringValue);
        }

        // if it is array then implement normal array deserialization
        if (JsonToken.BEGIN_ARRAY.equals(peek)) {
            in.beginArray();

            while (in.hasNext()) {
                String element = in.nextString();
                deserializedObject.add(element);
            }

            in.endArray();
        }

        return deserializedObject;
    }

}
