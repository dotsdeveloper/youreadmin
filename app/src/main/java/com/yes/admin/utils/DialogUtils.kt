package com.yes.admin.utils

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayoutMediator
import com.yes.admin.*
import com.yes.admin.adapter.ImageAdapter
import com.yes.admin.adapter.ProductQuantityAdapter
import com.yes.admin.adapter.ViewPagerAdapter
import com.yes.admin.databinding.DialogAddCategoryBinding
import com.yes.admin.databinding.DialogAddProductBinding
import com.yes.admin.databinding.DialogViewProductBinding
import com.yes.admin.interfaces.DialogCallBack
import com.yes.admin.interfaces.OnClickListener
import com.yes.admin.models.*
import com.yes.admin.network.ApiConnection
import com.yes.admin.session.Constants
import com.yes.admin.session.SharedHelper
import com.yes.admin.session.TempSingleton
import kotlin.math.roundToInt

@SuppressLint("UnspecifiedImmutableFlag")
object DialogUtils {
    private var loaderDialog: Dialog? = null
    fun showPictureDialog(activity: Activity, cimg: ImageView?,cimg1:ImageView?) {
        TempSingleton.getInstance().imageview1 = cimg
        TempSingleton.getInstance().imageview2 = cimg1
        val alertBuilder = AlertDialog.Builder(activity)
        alertBuilder.setTitle(activity.getString(R.string.choose_your_option))
        val items = arrayOf(activity.getString(R.string.gallery), activity.getString(R.string.camera))
        //val items = arrayOf("Gallery")
        alertBuilder.setItems(items) { _, which ->
            when (which) {
                0 -> if (BaseUtils.isPermissionsEnabled(activity, Constants.IntentKeys.GALLERY)) {
                    BaseUtils.openGallery(activity)
                } else {
                    if (BaseUtils.isDeniedPermission(activity, Constants.IntentKeys.GALLERY)) {
                        BaseUtils.permissionsEnableRequest(activity, Constants.IntentKeys.GALLERY)
                    } else {
                        BaseUtils.displayManuallyEnablePermissionsDialog(activity, Constants.IntentKeys.GALLERY, null)
                    }
                }
                1 -> if (BaseUtils.isPermissionsEnabled(activity, Constants.IntentKeys.CAMERA)) {
                    BaseUtils.openCamera(activity)
                } else {
                    if (BaseUtils.isDeniedPermission(activity, Constants.IntentKeys.CAMERA)) {
                        BaseUtils.permissionsEnableRequest(activity, Constants.IntentKeys.CAMERA)
                    } else {
                        BaseUtils.displayManuallyEnablePermissionsDialog(activity, Constants.IntentKeys.CAMERA, null)
                    }
                }
            }
        }

        val alert = alertBuilder.create()
        val window = alert.window
        if (window != null) {
            //window.attributes.windowAnimations = R.style.DialogAnimation
        }
        alert.show()
    }
    fun showLoader(context: Context) {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }

        loaderDialog = Dialog(context)
        loaderDialog?.setCancelable(false)
        loaderDialog?.setCanceledOnTouchOutside(false)
        loaderDialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        loaderDialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val inflater = LayoutInflater.from(context)
        val view = inflater?.inflate(R.layout.dialog_loader, null)
        if (view != null) {
            loaderDialog!!.setContentView(view)
        }

        loaderDialog!!.findViewById<com.airbnb.lottie.LottieAnimationView>(R.id.anim_view).playAnimation()
       // Glide.with(context).load(R.drawable.loader_common).into(loaderDialog!!.findViewById(R.id.image))

        if (!loaderDialog?.isShowing!!) {
            loaderDialog?.show()
        }
    }
    fun dismissLoader() {
        if (loaderDialog != null) {
            if (loaderDialog!!.isShowing) {
                loaderDialog!!.dismiss()
            }
        }
    }

    fun showViewCategoryDialog(context: Context, categoryListModel: CategoryListModel?,subCategoryListModel: SubCategoryListModel?,brandModel: BrandModel?,productUnitModel: ProductUnitModel?) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_view_category)
        dialog.window?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(context, R.color.transparent)))
        val width: Int = (context.resources.displayMetrics.widthPixels * 0.9).roundToInt()
        //var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()
        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        val headerTextView = dialog.findViewById<TextView>(R.id.top)
        val close = dialog.findViewById<ImageView>(R.id.close)
        val cimg = dialog.findViewById<ImageView>(R.id.cimag)
        val cname = dialog.findViewById<TextView>(R.id.cname)
        val cnametxt = dialog.findViewById<TextView>(R.id.cname_txt)
        val scname = dialog.findViewById<TextView>(R.id.scname)
        val status = dialog.findViewById<TextView>(R.id.status)
        val created = dialog.findViewById<TextView>(R.id.created)
        val cLinear = dialog.findViewById<LinearLayout>(R.id.c_linear)
        val scLinear = dialog.findViewById<LinearLayout>(R.id.sc_linear)
        if(categoryListModel != null){
            headerTextView.text = context.getString(R.string.category_details)
            cnametxt.text = context.getString(R.string.category_name)
            cname.text = categoryListModel.name
            scLinear.visibility = View.GONE
            created.text = BaseUtils.getFormattedDate(categoryListModel.created_at!!,Constants.ApiKeys.TIME_INPUT_FORMAT,"MMM dd, yyyy")
            if(BaseUtils.nullCheckerStr(categoryListModel.file).isNotEmpty()){
                UiUtils.loadImage(cimg,categoryListModel.file)
            }

            if(categoryListModel.status == 1){
                status.text = context.getString(R.string.active)
            }
            else if(categoryListModel.status == 0){
                status.text = context.getString(R.string.inactive)
            }
        }
        else if(subCategoryListModel != null){
            scLinear.visibility = View.VISIBLE
            headerTextView.text = context.getString(R.string.sub_category_details)
            scname.text = subCategoryListModel.name
            cname.text = subCategoryListModel.category!!.name
            created.text = BaseUtils.getFormattedDate(subCategoryListModel.created_at!!,Constants.ApiKeys.TIME_INPUT_FORMAT,"MMM dd, yyyy")
            if(BaseUtils.nullCheckerStr(subCategoryListModel.file).isNotEmpty()){
                UiUtils.loadImage(cimg,subCategoryListModel.file)
            }

            if(subCategoryListModel.status == 1){
                status.text = context.getString(R.string.active)
            }
            else if(subCategoryListModel.status == 0){
                status.text = context.getString(R.string.inactive)
            }
        }
        else if(brandModel != null){
            headerTextView.text = context.getString(R.string.brand_details)
            cnametxt.text = context.getString(R.string.brand_name)
            cname.text = brandModel.name
            scLinear.visibility = View.GONE
            created.text = BaseUtils.getFormattedDate(brandModel.created_at!!,Constants.ApiKeys.TIME_INPUT_FORMAT,"MMM dd, yyyy")
            if(BaseUtils.nullCheckerStr(brandModel.file).isNotEmpty()){
                UiUtils.loadImage(cimg,brandModel.file)
            }

            if(brandModel.status == 1){
                status.text = context.getString(R.string.active)
            }
            else if(brandModel.status == 0){
                status.text = context.getString(R.string.inactive)
            }
        }
        else if(productUnitModel != null){
            headerTextView.text = context.getString(R.string.measurement_details)
            cnametxt.text = context.getString(R.string.measurement_name)
            cname.text = productUnitModel.name
            scLinear.visibility = View.GONE
            created.text = BaseUtils.getFormattedDate(productUnitModel.created_at!!,Constants.ApiKeys.TIME_INPUT_FORMAT,"MMM dd, yyyy")

            if(productUnitModel.status == 1){
                status.text = context.getString(R.string.active)
            }
            else if(productUnitModel.status == 0){
                status.text = context.getString(R.string.inactive)
            }
        }

        close.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    fun showAddCategoryDialog(
        context: Context,
        activity: Activity,
        isCategory : Boolean,
        isSubcategory : Boolean,
        isBrand : Boolean,
        isUnit : Boolean,
        categoryListModel: CategoryListModel?,
        subCategoryListModel: SubCategoryListModel?,
        brandModel: BrandModel?,
        productUnitModel: ProductUnitModel?,
        viewLifecycleOwner: LifecycleOwner,
        callBack: OnClickListener
    ) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        val binding: DialogAddCategoryBinding = DialogAddCategoryBinding.inflate(dialog.layoutInflater)
        dialog.setContentView(R.layout.dialog_add_category)
        dialog.setContentView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(context, R.color.transparent)))
        val width: Int = (context.resources.displayMetrics.widthPixels * 0.9).roundToInt()
        //var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()
        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        val custypelist: ArrayList<String> = ArrayList()
        val catlist: ArrayList<String> = ArrayList()
        var customerType = 0
        var cid = 0
        when {
            isCategory -> {
                binding.category.root.visibility = View.VISIBLE
                binding.subCategory.root.visibility = View.GONE
                binding.brand.root.visibility = View.GONE
                binding.unit.root.visibility = View.GONE
                binding.category.close.setOnClickListener {
                    dialog.dismiss()
                }
                binding.category.cimag.setOnClickListener{
                    showPictureDialog(activity,binding.category.cimag,binding.category.imgClose)
                }
                binding.category.imgClose.setOnClickListener{
                    TempSingleton.getInstance().multipartFileBody = null
                    TempSingleton.getInstance().isFile = 2
                    binding.category.imgClose.visibility = View.GONE
                    UiUtils.loadDefaultImage(binding.category.cimag)
                }

                custypelist.add(0,"Select CustomerType")
                for ((index, value) in SharedHelper(context).customerTypeList.withIndex()) {
                    custypelist.add(value.name!!)
                }
                val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, custypelist)
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                binding.category.ctype.adapter = dataAdapter
                binding.category.ctype.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long,
                    ) {
                        val item: String = parent.getItemAtPosition(position).toString()
                        customerType = if(position == 0){
                            0
                        } else{
                            SharedHelper(context).customerTypeList[position-1].id!!
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        // sometimes you need nothing here
                    }
                }
                if(categoryListModel != null){
                    binding.category.title.text = context.getString(R.string.edit_category)
                    binding.category.cname.setText(categoryListModel.name)
                    binding.category.desc.setText(UiUtils.convertHtml(BaseUtils.nullCheckerStr(categoryListModel.information)))
                    binding.category.offerinfo.setText(categoryListModel.offer)
                    if(BaseUtils.nullCheckerStr(categoryListModel.file).isNotEmpty()){
                        UiUtils.loadImage(binding.category.cimag,categoryListModel.file)
                        binding.category.imgClose.visibility = View.VISIBLE
                    }
                    else{
                        binding.category.imgClose.visibility = View.GONE
                    }

                    if(categoryListModel.customer_type != null){
                        var pos = 0
                        for ((index, value) in SharedHelper(context).customerTypeList.withIndex()) {
                            if(categoryListModel.customer_type!!.id == value.id){
                                pos = index
                            }
                        }
                        binding.category.ctype.setSelection(pos+1)
                    }
                    else{
                        binding.category.ctype.setSelection(0)
                    }
                }
                else{
                    binding.category.ctype.setSelection(0)
                }

                binding.category.save.setOnClickListener{
                    showLoader(context)
                    if(categoryListModel != null){
                        ApiConnection.getInstance().updateCategory(context,
                            categoryListModel.id!!,
                            binding.category.cname.text.toString(),
                            customerType,
                            binding.category.desc.text.toString(),
                            binding.category.offerinfo.text.toString()).observe(viewLifecycleOwner) {
                            dismissLoader()
                            it?.let {
                                it.error.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(binding.root,it.status)
                                    } else{
                                        dialog.dismiss()
                                        callBack.onClickItem(it.data!!, SubCategoryListModel(), BrandModel(), ProductUnitModel())
                                    }
                                }
                            }
                        }
                    }
                    else{
                        ApiConnection.getInstance().addCategory(context,
                            binding.category.cname.text.toString(),
                            customerType,
                            binding.category.desc.text.toString(),
                            binding.category.offerinfo.text.toString()).observe(viewLifecycleOwner) {
                            dismissLoader()
                            it?.let {
                                it.error.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(binding.root,it.status)
                                    } else{
                                        dialog.dismiss()
                                        callBack.onClickItem(it.data!!, SubCategoryListModel(), BrandModel(), ProductUnitModel())
                                    }
                                }
                            }
                        }

                    }
                }
            }
            isSubcategory -> {
                binding.category.root.visibility = View.GONE
                binding.subCategory.root.visibility = View.VISIBLE
                binding.brand.root.visibility = View.GONE
                binding.unit.root.visibility = View.GONE
                binding.subCategory.close.setOnClickListener {
                    dialog.dismiss()
                }
                binding.subCategory.cimag.setOnClickListener{
                    showPictureDialog(activity,binding.subCategory.cimag,binding.subCategory.imgClose)
                }
                binding.subCategory.imgClose.setOnClickListener{
                    TempSingleton.getInstance().multipartFileBody = null
                    TempSingleton.getInstance().isFile = 2
                    binding.subCategory.imgClose.visibility = View.GONE
                    UiUtils.loadDefaultImage(binding.subCategory.cimag)
                }
                catlist.add(0,"Select Category")
                for ((index, value) in SharedHelper(context).categoryList.withIndex()) {
                    catlist.add(value.name!!)
                }
                val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, catlist)
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                binding.subCategory.clist.adapter = dataAdapter
                binding.subCategory.clist.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position: Int,
                        id: Long,
                    ) {
                        val item: String = parent.getItemAtPosition(position).toString()
                        cid = if(position == 0){
                            0
                        } else{
                            SharedHelper(context).categoryList[position-1].id!!
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        // sometimes you need nothing here
                    }
                }
                if(subCategoryListModel != null){
                    binding.subCategory.title.text = context.getString(R.string.edit_sub_category)
                    binding.subCategory.scname.setText(subCategoryListModel.name)
                    binding.subCategory.desc.setText(UiUtils.convertHtml(BaseUtils.nullCheckerStr(subCategoryListModel.information)))
                    if(BaseUtils.nullCheckerStr(subCategoryListModel.file).isNotEmpty()){
                        UiUtils.loadImage(binding.subCategory.cimag,subCategoryListModel.file)
                        binding.subCategory.imgClose.visibility = View.VISIBLE
                    }
                    else{
                        binding.subCategory.imgClose.visibility = View.GONE
                    }

                    if(subCategoryListModel.category_id != null){
                        var pos = 0
                        for ((index, value) in SharedHelper(context).categoryList.withIndex()) {
                            if(subCategoryListModel.category_id == value.id){
                                pos = index
                            }
                        }
                        binding.subCategory.clist.setSelection(pos+1)
                    }
                    else{
                        binding.subCategory.clist.setSelection(0)
                    }
                }
                else{
                    binding.subCategory.clist.setSelection(0)
                }

                binding.subCategory.save.setOnClickListener{
                    showLoader(context)
                    if(subCategoryListModel != null){
                        ApiConnection.getInstance().updateSubCategory(context,
                            subCategoryListModel.id!!,
                            binding.subCategory.scname.text.toString(),
                            cid,
                            binding.subCategory.desc.text.toString()).observe(viewLifecycleOwner) {
                            dismissLoader()
                            it?.let {
                                it.error.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(binding.root,it.status)
                                    } else{
                                        dialog.dismiss()
                                        callBack.onClickItem(CategoryListModel(),it.data!!, BrandModel(), ProductUnitModel())
                                    }
                                }
                            }
                        }
                    }
                    else{
                        ApiConnection.getInstance().addSubCategory(context,
                            binding.subCategory.scname.text.toString(),
                            cid,
                            binding.subCategory.desc.text.toString()).observe(viewLifecycleOwner) {
                            dismissLoader()
                            it?.let {
                                it.error.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(binding.root,it.status)
                                    } else{
                                        dialog.dismiss()
                                        callBack.onClickItem(CategoryListModel(),it.data!!, BrandModel(), ProductUnitModel())
                                    }
                                }
                            }
                        }

                    }
                }
            }
            isBrand -> {
                binding.category.root.visibility = View.GONE
                binding.subCategory.root.visibility = View.GONE
                binding.brand.root.visibility = View.VISIBLE
                binding.unit.root.visibility = View.GONE
                binding.brand.close.setOnClickListener {
                    dialog.dismiss()
                }
                binding.brand.cimag.setOnClickListener{
                    showPictureDialog(activity,binding.brand.cimag,binding.brand.imgClose)
                }
                binding.brand.imgClose.setOnClickListener{
                    TempSingleton.getInstance().multipartFileBody = null
                    TempSingleton.getInstance().isFile = 2
                    binding.brand.imgClose.visibility = View.GONE
                    UiUtils.loadDefaultImage(binding.category.cimag)
                }

                if(brandModel != null){
                    binding.brand.title.text = context.getString(R.string.edit_brand)
                    binding.brand.cname.setText(brandModel.name)
                    binding.brand.desc.setText(UiUtils.convertHtml(BaseUtils.nullCheckerStr(brandModel.description)))
                    if(BaseUtils.nullCheckerStr(brandModel.file).isNotEmpty()){
                        UiUtils.loadImage(binding.brand.cimag,brandModel.file)
                        binding.brand.imgClose.visibility = View.VISIBLE
                    }
                    else{
                        binding.brand.imgClose.visibility = View.GONE
                    }
                }
                else{

                }

                binding.brand.save.setOnClickListener{
                    showLoader(context)
                    if(brandModel != null){
                        ApiConnection.getInstance().updateBrand(context,
                            brandModel.id!!,
                            binding.brand.cname.text.toString(),
                            binding.brand.desc.text.toString()).observe(viewLifecycleOwner) {
                            dismissLoader()
                            it?.let {
                                it.error.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(binding.root,it.status)
                                    }
                                    else{
                                        dialog.dismiss()
                                        callBack.onClickItem(CategoryListModel(),
                                            SubCategoryListModel(), it.data!!, ProductUnitModel())
                                    }
                                }
                            }
                        }
                    }
                    else{
                        ApiConnection.getInstance().addBrand(context,
                            binding.brand.cname.text.toString(),
                            binding.brand.desc.text.toString()).observe(viewLifecycleOwner) {
                            dismissLoader()
                            it?.let {
                                it.error.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(binding.root,it.status)
                                    }
                                    else{
                                        dialog.dismiss()
                                        callBack.onClickItem(CategoryListModel(),SubCategoryListModel(), it.data!!, ProductUnitModel())
                                    }
                                }
                            }
                        }

                    }
                }
            }
            isUnit -> {
                binding.category.root.visibility = View.GONE
                binding.subCategory.root.visibility = View.GONE
                binding.brand.root.visibility = View.GONE
                binding.unit.root.visibility = View.VISIBLE
                binding.unit.close.setOnClickListener {
                    dialog.dismiss()
                }
                if(productUnitModel != null){
                    binding.unit.title.text = context.getString(R.string.edit_measurement)
                    binding.unit.cname.setText(productUnitModel.name)
                }
                else{

                }

                binding.unit.save.setOnClickListener{
                    showLoader(context)
                    if(productUnitModel != null){
                        ApiConnection.getInstance().updateMeasurement(context,
                            productUnitModel.id!!,
                            binding.unit.cname.text.toString()).observe(viewLifecycleOwner) {
                            dismissLoader()
                            it?.let {
                                it.error.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(binding.root,it.status)
                                    }
                                    else{
                                        dialog.dismiss()
                                        callBack.onClickItem(CategoryListModel(),
                                            SubCategoryListModel(), BrandModel(), it.data!!)
                                    }
                                }
                            }
                        }
                    }
                    else{
                        ApiConnection.getInstance().addMeasurement(context,
                            binding.unit.cname.text.toString()).observe(viewLifecycleOwner) {
                            dismissLoader()
                            it?.let {
                                it.error.let { error ->
                                    if (error) {
                                        UiUtils.showSnack(binding.root,it.status)
                                    }
                                    else{
                                        dialog.dismiss()
                                        callBack.onClickItem(CategoryListModel(),SubCategoryListModel(), BrandModel(), it.data!!)
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
        dialog.show()
    }

    fun showAddCustomerDialog(
        context: Context,
        activity: Activity,
        viewLifecycleOwner: LifecycleOwner,
        callBack: OnClickListener
    ) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        val binding: DialogAddCategoryBinding = DialogAddCategoryBinding.inflate(dialog.layoutInflater)
        dialog.setContentView(R.layout.dialog_add_category)
        dialog.setContentView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(context, R.color.transparent)))
        val width: Int = (context.resources.displayMetrics.widthPixels * 0.9).roundToInt()
        //var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()
        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        binding.category.root.visibility = View.GONE
        binding.subCategory.root.visibility = View.GONE
        binding.brand.root.visibility = View.GONE
        binding.unit.root.visibility = View.GONE
        binding.customer.root.visibility = View.VISIBLE
        binding.customer.close.setOnClickListener {
            dialog.dismiss()
        }
        binding.customer.save.setOnClickListener{
            showLoader(context)
            ApiConnection.getInstance().addCustomer(context,
                binding.customer.name.text.toString(),
                binding.customer.mobile.text.toString(),
                binding.customer.mailid.text.toString()).observe(viewLifecycleOwner) {
                        dismissLoader()
                        it?.let {
                            it.error.let { error ->
                                if (error) {
                                    UiUtils.showSnack(binding.root,it.status)
                                } else{
                                    dialog.dismiss()
                                    callBack.onClickItem()
                                }
                            }
                        }
                    }
            }
        dialog.show()
    }

    fun showAddProductDialog(
        context: Context,
        activity: Activity,
        supportFragmentManager: FragmentManager,
        lifecycle: Lifecycle,
        productModel: ProductModel?,
        callBack: OnClickListener
    ) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        val binding: DialogAddProductBinding = DialogAddProductBinding.inflate(dialog.layoutInflater)
       //dialog.setContentView(R.layout.dialog_add_product)
        dialog.setContentView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(context, R.color.transparent)))
        val width: Int = (context.resources.displayMetrics.widthPixels * 0.9).roundToInt()
        //var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()
        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        val tabArray = arrayOf("General", "Unit & Pricing", "Photos","Attributes")
        val viewPager = binding.viewPager
        val tabLayout = binding.tabLayout

        val adapter = ViewPagerAdapter(supportFragmentManager, lifecycle,productModel)
        viewPager.adapter = adapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = tabArray[position]
        }.attach()

        if(productModel != null){
            binding.title.text = context.getString(R.string.add_product)
        }
        else{
            binding.title.text = context.getString(R.string.edit_product)
        }
        binding.close.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    fun showViewProductDialog(
        context: Context,
        productModel: ProductModel,
    ) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        val binding: DialogViewProductBinding = DialogViewProductBinding.inflate(dialog.layoutInflater)
        dialog.setContentView(R.layout.dialog_view_product)
        dialog.setContentView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(context, R.color.transparent)))
        val width: Int = (context.resources.displayMetrics.widthPixels * 0.9).roundToInt()
        //var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()
        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        binding.close.setOnClickListener {
            dialog.dismiss()
        }
        binding.pname.text = productModel.name
        binding.cname.text = productModel.category!!.name
        binding.scname.text = productModel.subcategory!!.name
        binding.desc.text = productModel.description
        binding.created.text = BaseUtils.getFormattedDate(productModel.created_at!!,Constants.ApiKeys.TIME_INPUT_FORMAT,"MMM dd, yyyy")
        if(productModel.status == 1){
            binding.status.text = context.getString(R.string.active)
        }
        else if(productModel.status == 0){
            binding.status.text = context.getString(R.string.inactive)
        }

        if(productModel.is_recommend == 1){
            binding.rtc.text = context.getString(R.string.yes)
        }
        else{
            binding.rtc.text = context.getString(R.string.no)
        }

        if(productModel.home_display == 1){
            binding.dth.text = context.getString(R.string.yes)
        }
        else{
            binding.dth.text = context.getString(R.string.no)
        }

        if(productModel.enquiry == 1){
            binding.enquiry.text = context.getString(R.string.yes)
        }
        else{
            binding.enquiry.text = context.getString(R.string.no)
        }

        if(productModel.stock_available == 1){
            binding.stockAvailable.text = context.getString(R.string.yes)
        }
        else{
            binding.stockAvailable.text = context.getString(R.string.no)
        }

        if(productModel.filesNew.isNotEmpty()) {
            UiUtils.loadImage(binding.pimag, productModel.filesNew[0])
            binding.imageRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            binding.imageRecycler.adapter = ImageAdapter(context, productModel.filesNew)
        }
        else{
           // var files: Array<String> = arrayOf(String())
            val files = Files()
            files[0] = "empty"
            binding.imageRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            binding.imageRecycler.adapter = ImageAdapter(context, files)
        }

        if(productModel.quantities!!.isNotEmpty()){
            binding.qtyRecycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            binding.qtyRecycler.adapter = ProductQuantityAdapter(context, productModel.quantities!!)
        }
        else{
            binding.linearQty1.visibility = View.GONE
            binding.linearQty2.visibility = View.GONE
        }
        dialog.show()
    }

    fun showAlertDialog(context: Context, title: String, content: String, positiveText: String, negativeText: String, callBack: DialogCallBack) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        /*var width: Int = (context.resources.displayMetrics.widthPixels * 0.8).roundToInt()
    // var height: Int = (context.resources.displayMetrics.widthPixels * 0.6).roundToInt()

    dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
*/

        val headerTextView = dialog.findViewById<TextView>(R.id.header)
        val contentTextView = dialog.findViewById<TextView>(R.id.content)

        val cancel = dialog.findViewById<Button>(R.id.cancel)
        val ok = dialog.findViewById<Button>(R.id.ok)

        if (content == "") {
            contentTextView.visibility = View.GONE
        }

        headerTextView.text = title
        contentTextView.text = content

        cancel.text = negativeText
        ok.text = positiveText

        UiUtils.buttonBgColor(cancel, null, R.color.colorPrimary)
        UiUtils.buttonBgColor(ok,null, R.color.colorPrimary)

        cancel.setOnClickListener {
            callBack.onNegativeClick()
            dialog.dismiss()
        }

        ok.setOnClickListener {
            callBack.onPositiveClick("")
            dialog.dismiss()
        }

        dialog.show()

    }
    /*fun showAlert(context: Context, singleTapListener: SingleTapListener, content: String) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        val contentView = dialog.findViewById<TextView>(R.id.content)
        val ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content



        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        val width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)
        dialog.setOnCancelListener {
            singleTapListener.singleTap()
        }

        dialog.show()

    }
    fun showAlertWithHeader(context: Context, singleTapListener: SingleTapListener, content: String, headerVal: String) {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.setContentView(R.layout.dialog_alert_single)
        dialog.window?.setBackgroundDrawable(
            ColorDrawable(
                ContextCompat.getColor(
                    context,
                    R.color.transparent
                )
            )
        )

        val contentView = dialog.findViewById<TextView>(R.id.content)
        val header = dialog.findViewById<TextView>(R.id.header)
        val ok = dialog.findViewById<TextView>(R.id.ok)

        contentView.text = content
        header.text = headerVal


        ok.setOnClickListener {

            dialog.dismiss()
            singleTapListener.singleTap()
        }

        val width: Int = (context.resources.displayMetrics.widthPixels * 0.7).roundToInt()

        dialog.window?.setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT)

        dialog.show()

    }
*/
}