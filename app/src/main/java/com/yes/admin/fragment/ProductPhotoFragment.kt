package com.yes.admin.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.adapter.CategoryAdapter
import com.yes.admin.databinding.FragmentProductPhotoBinding
import com.yes.admin.models.*
import com.yes.admin.session.Constants
import com.yes.admin.session.TempSingleton
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils

class ProductPhotoFragment : Fragment() {
    private var _binding: FragmentProductPhotoBinding? = null
    private val binding get() = _binding!!
    lateinit var dashBoardActivity: DashBoardActivity
    lateinit var adapter: CategoryAdapter
    var data: ArrayList<ProductUnitModel> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductPhotoBinding.inflate(inflater, container, false)
        val root: View = binding.root
        dashBoardActivity = (activity as DashBoardActivity)
        load()
        return root
    }

    private fun load(){
        binding.cimag.setOnClickListener {
            DialogUtils.showPictureDialog(requireActivity(),binding.cimag,binding.imgClose)
        }

        binding.imgClose.setOnClickListener {
            TempSingleton.getInstance().multipartFileBody = null
            TempSingleton.getInstance().isFile = 2
            binding.imgClose.visibility = View.GONE
            UiUtils.loadDefaultImage(binding.cimag)
        }

        if(arguments != null) {
            val files = requireArguments().getSerializable(Constants.IntentKeys.FILES) as Array<String>
            if(files.isNotEmpty()){
                UiUtils.loadImage(binding.cimag,files[0])
            }
        }
    }

}