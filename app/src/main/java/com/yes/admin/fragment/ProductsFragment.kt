package com.yes.admin.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.admin.R
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.adapter.ProductAdapter
import com.yes.admin.databinding.FragmentProductsBinding
import com.yes.admin.helpers.RecyclerTouchListener
import com.yes.admin.interfaces.OnClickListener
import com.yes.admin.models.*
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils

class ProductsFragment : Fragment() {
    private var _binding: FragmentProductsBinding? = null
    private val binding get() = _binding!!
    lateinit var dashBoardActivity: DashBoardActivity
    lateinit var adapter: ProductAdapter
    var data: ArrayList<ProductModel> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        dashBoardActivity = (activity as DashBoardActivity)
        DialogUtils.showLoader(requireContext())
        dashBoardActivity.apiConnection.getProducts(requireContext()).observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        loadRecycler()
                    }
                    else {
                        data = it.data!!
                        loadRecycler()
                    }
                }
            }
        }

        binding.add.setOnClickListener {
            DialogUtils.showAddProductDialog(requireContext(),
                requireActivity(),
                dashBoardActivity.supportFragmentManager,
                dashBoardActivity.lifecycle,
                null,
                object : OnClickListener {
                    override fun onClickItem(
                        data1: CategoryListModel,
                        data2: SubCategoryListModel,
                        data3: BrandModel,
                        data4: ProductUnitModel,
                    ) {

                    }

                    override fun onClickItem() {
                    }
                })
        }
        return root
    }

    fun loadRecycler(){
        if(data.isEmpty()){
            binding.top.visibility = View.GONE
            binding.recyclerProduct.visibility = View.GONE
        }
        else{
            binding.top.visibility = View.VISIBLE
            binding.recyclerProduct.visibility = View.VISIBLE
        }
        binding.recyclerProduct.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        adapter = ProductAdapter(dashBoardActivity,requireContext(), data)
        binding.recyclerProduct.adapter = adapter
        val touchListener = RecyclerTouchListener(requireActivity(), binding.recyclerProduct)
        touchListener.setClickable(object :
            RecyclerTouchListener.OnRowClickListener {
            override fun onRowClicked(position: Int) {
                // Toast.makeText(requireActivity(), position, Toast.LENGTH_SHORT).show()
            }

            override fun onIndependentViewClicked(independentViewID: Int, position: Int, ) {

            }
        })
            .setSwipeOptionViews(R.id.delete_task, R.id.edit_task)
            .setSwipeable(R.id.rowFG, R.id.rowBG,
                RecyclerTouchListener.OnSwipeOptionsClickListener { viewID, position ->
                    when (viewID) {
                        R.id.delete_task -> {
                            // taskList.remove(position)
                            //adapter.setTaskList(taskList)
                            dashBoardActivity.apiConnection.deleteProduct(
                                requireContext(),
                                data[position].id!!).observe(viewLifecycleOwner) {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error.let { error ->
                                        if (error) {
                                            UiUtils.showSnack(binding.root,
                                                it.status)
                                        } else {
                                            data.removeAt(position)
                                            adapter.notifyItemRemoved(position)
                                        }
                                    }
                                }
                            }
                        }
                        R.id.edit_task -> {
                            dashBoardActivity.apiConnection.updateProductStatus(
                                requireContext(),
                                data[position].id!!).observe(viewLifecycleOwner) {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error.let { error ->
                                        if (error) {
                                            UiUtils.showSnack(binding.root,
                                                it.status)
                                        } else {
                                            data[position].status = it.data?.get(0)!!.status
                                            adapter.notifyItemChanged(position)
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
        binding.recyclerProduct.addOnItemTouchListener(touchListener)
    }

}