package com.yes.admin.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.admin.R
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.adapter.OrdersAdapter
import com.yes.admin.databinding.FragmentOrdersBinding
import com.yes.admin.helpers.RecyclerTouchListener
import com.yes.admin.models.OrderListModel
import com.yes.admin.utils.BaseUtils
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils

class OrdersFragment : Fragment() {
    private var _binding: FragmentOrdersBinding? = null
    private val binding get() = _binding!!
    lateinit var dashBoardActivity: DashBoardActivity
    lateinit var adapter: OrdersAdapter
    var data: ArrayList<OrderListModel> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentOrdersBinding.inflate(inflater, container, false)
        val root: View = binding.root
        dashBoardActivity = (activity as DashBoardActivity)
        DialogUtils.showLoader(requireContext())
        dashBoardActivity.apiConnection.getOrders(requireContext()).observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        loadRecycler(it.data!!)
                    }
                    else {
                        data = it.data!!
                        loadRecycler(it.data!!)
                    }
                }
            }
        }

        binding.search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                search(s.toString())
            }
            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })
        binding.search.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                BaseUtils.closeKeyBoard(binding.search,requireContext())
                //search()
                return@OnEditorActionListener true
            }
            false
        })
        binding.close.setOnClickListener {
            binding.search.setText("")
        }

        val custypelist: ArrayList<String> = ArrayList()
        custypelist.add(0,"Select Sort Field")
        custypelist.add("Order No")
        custypelist.add("Customer Info")
        custypelist.add("Payment Status")
        custypelist.add("Status")
        custypelist.add("Order Date")
        custypelist.add("Order Value")

        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, custypelist)
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.filter.adapter = dataAdapter
        binding.filter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long,
            ) {
                when (parent.getItemAtPosition(position).toString()) {
                    "Order No" -> {
                        val ndata: ArrayList<OrderListModel> = ArrayList()
                        ndata.addAll(data)
                        ndata.sortBy { it.invoice  }
                        loadRecycler(ndata)
                    }
                    "Customer Info" -> {
                        val ndata: ArrayList<OrderListModel> = ArrayList()
                        ndata.addAll(data)
                        ndata.sortBy { it.customer!!.name  }
                        loadRecycler(ndata)
                    }
                    "Payment Status" -> {
                        loadRecycler(data)
                    }
                    "Status" -> {
                        loadRecycler(data)
                    }
                    "Order Date" -> {
                        val ndata: ArrayList<OrderListModel> = ArrayList()
                        ndata.addAll(data)
                        ndata.sortBy { it.created_at  }
                        loadRecycler(ndata)
                    }
                    "Order Value" -> {
                        val ndata: ArrayList<OrderListModel> = ArrayList()
                        ndata.addAll(data)
                        ndata.sortBy { it.grand_total  }
                        loadRecycler(ndata)
                    }
                    else -> {
                        loadRecycler(data)
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // sometimes you need nothing here
            }
        }

        return root
    }

    private fun loadRecycler(orderdata: ArrayList<OrderListModel>) {
        if(orderdata.isEmpty()){
            binding.recyclerOrders.visibility = View.GONE
        }
        else{
            binding.recyclerOrders.visibility = View.VISIBLE
        }
        binding.recyclerOrders.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        adapter = OrdersAdapter(dashBoardActivity,requireContext(), orderdata)
        binding.recyclerOrders.adapter = adapter
        val touchListener = RecyclerTouchListener(requireActivity(), binding.recyclerOrders)
        touchListener.setClickable(object :
            RecyclerTouchListener.OnRowClickListener {
            override fun onRowClicked(position: Int) {
                // Toast.makeText(requireActivity(), position, Toast.LENGTH_SHORT).show()
            }

            override fun onIndependentViewClicked(independentViewID: Int, position: Int, ) {

            }
        })
            .setSwipeOptionViews(R.id.delete_task, R.id.edit_task)
            .setSwipeable(R.id.rowFG, R.id.rowBG,
                RecyclerTouchListener.OnSwipeOptionsClickListener { viewID, position ->
                    when (viewID) {
                        R.id.delete_task -> {
                            // taskList.remove(position)
                            //adapter.setTaskList(taskList)
                            /*dashBoardActivity.apiConnection.deleteSubCategory(
                                requireContext(),
                                data[position].id!!).observe(viewLifecycleOwner) {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error.let { error ->
                                        if (error) {
                                            UiUtils.showSnack(binding.root,
                                                it.status)
                                        } else {
                                            data.removeAt(position)
                                            adapter.notifyItemRemoved(position)
                                        }
                                    }
                                }
                            }*/
                        }
                        R.id.edit_task -> {
                           /* dashBoardActivity.apiConnection.updateSubCategoryStatus(
                                requireContext(),
                                data[position].id!!).observe(viewLifecycleOwner) {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error.let { error ->
                                        if (error) {
                                            UiUtils.showSnack(binding.root,
                                                it.status)
                                        } else {
                                            data[position].status = it.data!!.status
                                            adapter.notifyItemChanged(position)
                                        }
                                    }
                                }
                            }*/
                        }
                    }
                })
        binding.recyclerOrders.addOnItemTouchListener(touchListener)
    }

    fun search(s: String) {
        if(s.isNotEmpty()){
            val ndata: ArrayList<OrderListModel> = ArrayList()
            for(items in data){
                if(items.invoice!!.contains(s) ||
                    items.customer!!.name!!.contains(s) ||
                    items.customer!!.mobile!!.contains(s) ||
                    items.grand_total!!.contains(s)){
                    ndata.add(items)
                }
            }
            loadRecycler(ndata)
        }
        else{
            loadRecycler(data)
        }
    }

}