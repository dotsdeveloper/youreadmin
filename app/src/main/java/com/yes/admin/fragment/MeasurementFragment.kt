package com.yes.admin.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.admin.R
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.adapter.CategoryAdapter
import com.yes.admin.databinding.FragmentMeasurementBinding
import com.yes.admin.helpers.RecyclerTouchListener
import com.yes.admin.interfaces.OnClickListener
import com.yes.admin.models.BrandModel
import com.yes.admin.models.CategoryListModel
import com.yes.admin.models.ProductUnitModel
import com.yes.admin.models.SubCategoryListModel
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils

class MeasurementFragment : Fragment() {
    private var _binding: FragmentMeasurementBinding? = null
    private val binding get() = _binding!!
    lateinit var dashBoardActivity: DashBoardActivity
    lateinit var adapter: CategoryAdapter
    var data: ArrayList<ProductUnitModel> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentMeasurementBinding.inflate(inflater, container, false)
        val root: View = binding.root
        dashBoardActivity = (activity as DashBoardActivity)
        load()
        binding.add.setOnClickListener {
            DialogUtils.showAddCategoryDialog(requireContext(),requireActivity(),false,false,false,true,null,null,null,null,viewLifecycleOwner,
                object : OnClickListener {
                    override fun onClickItem(data1: CategoryListModel, data2: SubCategoryListModel, data3: BrandModel, data4: ProductUnitModel) {
                        data.add(data4)
                        loadRecycler()
                        dashBoardActivity.load()
                    }

                    override fun onClickItem() {
                    }
                })
        }
        return root
    }

    private fun load(){
        DialogUtils.showLoader(requireContext())
        dashBoardActivity.apiConnection.getMeasurement(requireContext()).observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        loadRecycler()
                    }
                    else {
                        data = it.data!!
                        loadRecycler()
                    }
                }
            }
        }
    }

    fun loadRecycler(){
        if(data.isEmpty()){
            binding.top.visibility = View.GONE
            binding.recyclerUnit.visibility = View.GONE
        }
        else{
            binding.top.visibility = View.VISIBLE
            binding.recyclerUnit.visibility = View.VISIBLE
        }
        binding.recyclerUnit.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        adapter = CategoryAdapter(dashBoardActivity,requireContext(), ArrayList(),ArrayList(), ArrayList(),data)
        binding.recyclerUnit.adapter = adapter
        val touchListener = RecyclerTouchListener(requireActivity(), binding.recyclerUnit)
        touchListener.setClickable(object :
            RecyclerTouchListener.OnRowClickListener {
            override fun onRowClicked(position: Int) {
                // Toast.makeText(requireActivity(), position, Toast.LENGTH_SHORT).show()
            }

            override fun onIndependentViewClicked(independentViewID: Int, position: Int, ) {

            }
        })
            .setSwipeOptionViews(R.id.delete_task, R.id.edit_task)
            .setSwipeable(R.id.rowFG, R.id.rowBG,
                RecyclerTouchListener.OnSwipeOptionsClickListener { viewID, position ->
                    when (viewID) {
                        R.id.delete_task -> {
                            // taskList.remove(position)
                            //adapter.setTaskList(taskList)
                            dashBoardActivity.apiConnection.deleteMeasurement(
                                requireContext(),
                                data[position].id!!).observe(viewLifecycleOwner) {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error.let { error ->
                                        if (error) {
                                            UiUtils.showSnack(binding.root,
                                                it.status)
                                        } else {
                                            data.removeAt(position)
                                            adapter.notifyItemRemoved(position)
                                        }
                                    }
                                }
                            }
                        }
                        R.id.edit_task -> {
                            dashBoardActivity.apiConnection.updateMeasurementStatus(
                                requireContext(),
                                data[position].id!!).observe(viewLifecycleOwner) {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error.let { error ->
                                        if (error) {
                                            UiUtils.showSnack(binding.root,
                                                it.status)
                                        } else {
                                            data[position].status = it.data!!.status
                                            adapter.notifyItemChanged(position)
                                            load()
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
        binding.recyclerUnit.addOnItemTouchListener(touchListener)
    }

}