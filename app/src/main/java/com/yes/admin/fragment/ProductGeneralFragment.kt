package com.yes.admin.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.databinding.FragmentProductGeneralBinding
import com.yes.admin.network.ApiConnection
import com.yes.admin.session.Constants
import com.yes.admin.session.SharedHelper
import com.yes.admin.session.TempSingleton
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils

class ProductGeneralFragment : Fragment() {
    private var _binding: FragmentProductGeneralBinding? = null
    private val binding get() = _binding!!
    lateinit var dashBoardActivity: DashBoardActivity
    lateinit var sharedHelper: SharedHelper
    val catlist: ArrayList<String> = ArrayList()
    val scatlist: ArrayList<String> = ArrayList()
    val brandlist: ArrayList<String> = ArrayList()
    var cid = 0
    var scid = 0
    var bid = 0
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductGeneralBinding.inflate(inflater, container, false)
        val root: View = binding.root
        dashBoardActivity = (activity as DashBoardActivity)
        sharedHelper = dashBoardActivity.sharedHelper
        load()
        binding.save.setOnClickListener {
            TempSingleton.getInstance().pname = binding.pname.text.toString()
            TempSingleton.getInstance().cid = cid
            TempSingleton.getInstance().scid = scid
            TempSingleton.getInstance().bid = bid
            TempSingleton.getInstance().desc = binding.desc.text.toString()
            DialogUtils.showLoader(requireContext())
            ApiConnection.getInstance().addProduct(requireContext()).observe(viewLifecycleOwner) {
                DialogUtils.dismissLoader()
                it?.let {
                    it.error.let { error ->
                        if (error) {
                            UiUtils.showSnack(binding.root,it.status)
                        }
                        else{

                        }
                    }
                }
            }
        }
        return root
    }

    private fun load(){
        catlist.add(0,"Select Category")
        for ((index, value) in sharedHelper.categoryList.withIndex()) {
            catlist.add(value.name!!)
        }
        val dataAdapter1: ArrayAdapter<String> = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, catlist)
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.catSpinner.adapter = dataAdapter1
        binding.catSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long,
            ) {
                val item: String = parent.getItemAtPosition(position).toString()
                cid = if(position == 0){
                    0
                } else{
                    sharedHelper.categoryList[position-1].id!!
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // sometimes you need nothing here
            }
        }

        scatlist.add(0,"Select Sub Category")
        for ((index, value) in sharedHelper.subcategoryList.withIndex()) {
            scatlist.add(value.name!!)
        }
        val dataAdapter2: ArrayAdapter<String> = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, scatlist)
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.scatSpinner.adapter = dataAdapter2
        binding.scatSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long,
            ) {
                val item: String = parent.getItemAtPosition(position).toString()
                scid = if(position == 0){
                    0
                } else{
                    sharedHelper.subcategoryList[position-1].id!!
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // sometimes you need nothing here
            }
        }

        brandlist.add(0,"Select Brand")
        for ((index, value) in sharedHelper.brandList.withIndex()) {
            brandlist.add(value.name!!)
        }
        val dataAdapter3: ArrayAdapter<String> = ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, brandlist)
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.brandSpinner.adapter = dataAdapter3
        binding.brandSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long,
            ) {
                val item: String = parent.getItemAtPosition(position).toString()
                bid = if(position == 0){
                    0
                } else{
                    sharedHelper.brandList[position-1].id!!
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // sometimes you need nothing here
            }
        }

        if(arguments != null){
            binding.pname.setText(requireArguments().getString(Constants.IntentKeys.PRODUCT))
            binding.desc.setText(requireArguments().getString(Constants.IntentKeys.DESCRIPTION))
            for ((index, value) in sharedHelper.brandList.withIndex()) {
                if(value.id == requireArguments().getInt(Constants.IntentKeys.BID)){
                   binding.brandSpinner.setSelection(index+1)
                }
            }

            for ((index, value) in sharedHelper.categoryList.withIndex()) {
                if(value.id == requireArguments().getInt(Constants.IntentKeys.CID)){
                    binding.catSpinner.setSelection(index+1)
                }
            }

            for ((index, value) in sharedHelper.subcategoryList.withIndex()) {
                if(value.id == requireArguments().getInt(Constants.IntentKeys.S_C_ID)){
                    binding.scatSpinner.setSelection(index+1)
                }
            }
        }
    }

}