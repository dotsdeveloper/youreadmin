package com.yes.admin.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.admin.R
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.adapter.ProfileAdapter
import com.yes.admin.databinding.FragmentProfileBinding
import com.yes.admin.helpers.RecyclerTouchListener
import com.yes.admin.interfaces.OnClickListener
import com.yes.admin.models.*
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils

class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    lateinit var dashBoardActivity: DashBoardActivity
    lateinit var adapter: ProfileAdapter
    var data: ArrayList<CustomerModel> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        val root: View = binding.root
        dashBoardActivity = (activity as DashBoardActivity)
        load()
        binding.add.setOnClickListener {
            DialogUtils.showAddCustomerDialog(requireContext(),requireActivity(),viewLifecycleOwner,
                object : OnClickListener {
                    override fun onClickItem(data1: CategoryListModel, data2: SubCategoryListModel, data3: BrandModel, data4: ProductUnitModel) {

                    }
                    override fun onClickItem() {
                        load()
                    }
                })
        }


        return root
    }

    private fun load(){
        DialogUtils.showLoader(requireContext())
        dashBoardActivity.apiConnection.getCustomers(requireContext()).observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        loadRecycler()
                    }
                    else {
                        data = it.data!!
                        loadRecycler()
                    }
                }
            }
        }
    }

    private fun loadRecycler(){
        if(data.isEmpty()){
            binding.top.visibility = View.GONE
            binding.recyclerProfile.visibility = View.GONE
        }
        else{
            binding.top.visibility = View.VISIBLE
            binding.recyclerProfile.visibility = View.VISIBLE
        }
        binding.recyclerProfile.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        adapter = ProfileAdapter(dashBoardActivity,requireContext(),data)
        binding.recyclerProfile.adapter = adapter
        val touchListener = RecyclerTouchListener(requireActivity(), binding.recyclerProfile)
        touchListener.setClickable(object : RecyclerTouchListener.OnRowClickListener {
            override fun onRowClicked(position: Int) {
                // Toast.makeText(requireActivity(), position, Toast.LENGTH_SHORT).show()
            }

            override fun onIndependentViewClicked(independentViewID: Int, position: Int, ) {

            }
        })
            .setSwipeOptionViews(R.id.delete_task, R.id.edit_task)
            .setSwipeable(R.id.rowFG, R.id.rowBG,
                RecyclerTouchListener.OnSwipeOptionsClickListener { viewID, position ->
                    when (viewID) {
                        R.id.delete_task -> {
                            // taskList.remove(position)
                            //adapter.setTaskList(taskList)

                        }
                        R.id.edit_task -> {

                        }
                    }
                })
        binding.recyclerProfile.addOnItemTouchListener(touchListener)
    }

}