package com.yes.admin.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.adapter.ProductAddQuantityAdapetr
import com.yes.admin.databinding.FragmentProductUnitBinding
import com.yes.admin.models.QuantitiesModel
import com.yes.admin.session.Constants
import com.yes.admin.session.SharedHelper
import com.yes.admin.session.TempSingleton
import org.json.JSONArray
import org.json.JSONObject

class ProductUnitFragment : Fragment() {
    private var _binding: FragmentProductUnitBinding? = null
    val binding get() = _binding!!
    lateinit var dashBoardActivity: DashBoardActivity
    lateinit var sharedHelper: SharedHelper
    val unitlist: ArrayList<String> = ArrayList()
    var jsonArray:JSONArray = JSONArray()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductUnitBinding.inflate(inflater, container, false)
        val root: View = binding.root
        dashBoardActivity = (activity as DashBoardActivity)
        sharedHelper = dashBoardActivity.sharedHelper
        binding.addQty.setOnClickListener {
            val jsonObject = JSONObject()
            jsonObject.put("unit_id","")
            jsonObject.put("quantity","")
            jsonObject.put("amount","")
            jsonObject.put("offer_price","")
            jsonObject.put("minimum_stock","0")
            jsonObject.put("is_loose","0")
            jsonObject.put("is_visible","0")
            jsonArray.put(jsonObject)
            load()
        }
        binding.save.setOnClickListener {
            TempSingleton.getInstance().quantities = jsonArray
        }
        load()

        if(arguments != null) {
            val quantities = requireArguments().getSerializable(Constants.IntentKeys.QUANTITIES) as ArrayList<QuantitiesModel>
            if(quantities.isNotEmpty()){
                for(items in quantities){
                    val jsonObject = JSONObject()
                    jsonObject.put("unit_id",items.unit_id)
                    jsonObject.put("quantity",items.quantity)
                    jsonObject.put("amount",items.amount)
                    jsonObject.put("offer_price",items.offer_price)
                    jsonObject.put("minimum_stock",items.minimum_stock)
                    jsonObject.put("is_loose",items.is_loose)
                    jsonObject.put("is_visible",items.is_visible)
                    jsonArray.put(jsonObject)
                }
                load()
            }
        }
        return root
    }

    private fun load(){
        binding.save.visibility = View.GONE
        unitlist.clear()
        unitlist.add(0,"Select Category")
        for ((index, value) in sharedHelper.productUnitList.withIndex()) {
            unitlist.add(value.name!!)
        }

        binding.recyclerProductUnit.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerProductUnit.adapter = ProductAddQuantityAdapetr(requireContext(),this,unitlist,jsonArray)
    }

}