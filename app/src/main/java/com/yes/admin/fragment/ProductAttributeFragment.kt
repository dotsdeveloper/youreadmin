package com.yes.admin.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.adapter.CategoryAdapter
import com.yes.admin.databinding.FragmentProductAttributeBinding
import com.yes.admin.models.ProductUnitModel

class ProductAttributeFragment : Fragment() {
    private var _binding: FragmentProductAttributeBinding? = null
    private val binding get() = _binding!!
    lateinit var dashBoardActivity: DashBoardActivity
    lateinit var adapter: CategoryAdapter
    var data: ArrayList<ProductUnitModel> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductAttributeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        dashBoardActivity = (activity as DashBoardActivity)
        return root
    }
}