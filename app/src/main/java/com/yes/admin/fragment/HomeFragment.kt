package com.yes.admin.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.yes.admin.R
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.adapter.CategoryAdapter
import com.yes.admin.databinding.FragmentHomeBinding
import com.yes.admin.models.BrandModel
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val DESKTOP_USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36"
    private val MOBILE_USER_AGENT = "Mozilla/5.0 (Linux; Android 9; Redmi 6 Pro Build/PKQ1.180917.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/89.0.4389.105 Mobile Safari/537.36"
    lateinit var dashBoardActivity: DashBoardActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
       /* val webpage = "https://yourestore.in/seller/"
        val webView = binding.webview
        webView.settings.javaScriptEnabled = true
        webView.settings.loadsImagesAutomatically = true
        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        webView.settings.domStorageEnabled = true
        webView.settings.builtInZoomControls = true
        webView.loadUrl(webpage)
        var userAgent: String = webView.settings.userAgentString
        try {
            val androidString: String = webView.settings.userAgentString.substring(userAgent.indexOf("("), userAgent.indexOf(")") + 1)
            userAgent = webView.settings.userAgentString.replace(androidString, "X11; Linux x86_64")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        webView.settings.userAgentString = userAgent
        webView.reload()*/

        dashBoardActivity = (activity as DashBoardActivity)
        binding.title.text = "Welcome back,"+dashBoardActivity.sharedHelper.name
        DialogUtils.showLoader(requireContext())
        dashBoardActivity.apiConnection.getAllCounts(requireContext()).observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                    }
                    else {
                        binding.turnover.text = "₹ "+it.data!!.order_value_last_6_month.toString()+".00"
                        binding.order.text = it.data!!.current_month_order.toString()
                        binding.category.text = it.data!!.category_count.toString()
                        binding.subCategory.text = it.data!!.sub_category_count.toString()
                        binding.product.text = it.data!!.product_count.toString()
                        binding.orders.text = it.data!!.order_count.toString()
                        binding.customers.text = it.data!!.customer_count.toString()
                        binding.sms.text = it.data!!.sms_count.toString()
                    }
                }
            }
        }

        binding.cardCategory.setOnClickListener {
            dashBoardActivity.navController.navigate(R.id.nav_category)
        }
        binding.cardSubCategory.setOnClickListener {
            dashBoardActivity.navController.navigate(R.id.nav_subcategory)
        }
        binding.cardProduct.setOnClickListener {
            dashBoardActivity.navController.navigate(R.id.nav_products)
        }
        binding.cardOrders.setOnClickListener {
            dashBoardActivity.navController.navigate(R.id.nav_orders)
        }
        binding.cardCustomer.setOnClickListener {
            dashBoardActivity.navController.navigate(R.id.nav_profile)
        }

        return root
    }

    fun setDesktopMode(webView: WebView, enabled: Boolean) {
        var newUserAgent: String? = webView.settings.userAgentString
        if (enabled) {
            try {
                val ua: String = webView.settings.userAgentString
                val androidOSString: String = webView.settings.userAgentString.substring(
                    ua.indexOf("("),
                    ua.indexOf(")") + 1
                )
                newUserAgent = webView.settings.userAgentString.replace(androidOSString, "(X11; Linux x86_64)")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            newUserAgent = null
        }
        webView.settings.apply {
            userAgentString = newUserAgent
            useWideViewPort = enabled
            loadWithOverviewMode = enabled
        }
        webView.reload()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}