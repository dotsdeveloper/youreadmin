package com.yes.admin.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.admin.R
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.adapter.CategoryAdapter
import com.yes.admin.databinding.FragmentSubCategoryBinding
import com.yes.admin.helpers.RecyclerTouchListener
import com.yes.admin.interfaces.OnClickListener
import com.yes.admin.models.BrandModel
import com.yes.admin.models.CategoryListModel
import com.yes.admin.models.ProductUnitModel
import com.yes.admin.models.SubCategoryListModel
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils

class SubCategoryFragment : Fragment() {
    private var _binding: FragmentSubCategoryBinding? = null
    private val binding get() = _binding!!
    lateinit var dashBoardActivity: DashBoardActivity
    lateinit var adapter: CategoryAdapter
    var data: ArrayList<SubCategoryListModel> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentSubCategoryBinding.inflate(inflater, container, false)
        val root: View = binding.root
        dashBoardActivity = (activity as DashBoardActivity)
        DialogUtils.showLoader(requireContext())
        dashBoardActivity.apiConnection.getSubCategory(requireContext()).observe(viewLifecycleOwner) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        loadRecycler()
                    }
                    else {
                        data = it.data!!
                        loadRecycler()
                    }
                }
            }
        }

        binding.add.setOnClickListener {
            DialogUtils.showAddCategoryDialog(requireContext(),requireActivity(),false,true,false,false,null,null,null,null,viewLifecycleOwner,
                object : OnClickListener {
                    override fun onClickItem(data1: CategoryListModel, data2: SubCategoryListModel, data3: BrandModel, data4: ProductUnitModel) {
                        data.add(data2)
                        loadRecycler()
                        dashBoardActivity.load()
                    }

                    override fun onClickItem() {
                    }
                })
        }
        return root
    }

    fun loadRecycler(){
        if(data.isEmpty()){
            binding.top.visibility = View.GONE
            binding.recyclerSubCategory.visibility = View.GONE
        }
        else{
            binding.top.visibility = View.VISIBLE
            binding.recyclerSubCategory.visibility = View.VISIBLE
        }
        binding.recyclerSubCategory.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        adapter = CategoryAdapter(dashBoardActivity,requireContext(), ArrayList(),data,ArrayList(), ArrayList())
        binding.recyclerSubCategory.adapter = adapter
        val touchListener = RecyclerTouchListener(requireActivity(), binding.recyclerSubCategory)
        touchListener.setClickable(object :
            RecyclerTouchListener.OnRowClickListener {
            override fun onRowClicked(position: Int) {
                // Toast.makeText(requireActivity(), position, Toast.LENGTH_SHORT).show()
            }

            override fun onIndependentViewClicked(independentViewID: Int, position: Int, ) {

            }
        })
            .setSwipeOptionViews(R.id.delete_task, R.id.edit_task)
            .setSwipeable(R.id.rowFG, R.id.rowBG,
                RecyclerTouchListener.OnSwipeOptionsClickListener { viewID, position ->
                    when (viewID) {
                        R.id.delete_task -> {
                            // taskList.remove(position)
                            //adapter.setTaskList(taskList)
                            dashBoardActivity.apiConnection.deleteSubCategory(
                                requireContext(),
                                data[position].id!!).observe(viewLifecycleOwner) {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error.let { error ->
                                        if (error) {
                                            UiUtils.showSnack(binding.root,
                                                it.status)
                                        } else {
                                            data.removeAt(position)
                                            adapter.notifyItemRemoved(position)
                                        }
                                    }
                                }
                            }
                        }
                        R.id.edit_task -> {
                            dashBoardActivity.apiConnection.updateSubCategoryStatus(
                                requireContext(),
                                data[position].id!!).observe(viewLifecycleOwner) {
                                DialogUtils.dismissLoader()
                                it?.let {
                                    it.error.let { error ->
                                        if (error) {
                                            UiUtils.showSnack(binding.root,
                                                it.status)
                                        } else {
                                            data[position].status = it.data!!.status
                                            adapter.notifyItemChanged(position)
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
        binding.recyclerSubCategory.addOnItemTouchListener(touchListener)
    }

}