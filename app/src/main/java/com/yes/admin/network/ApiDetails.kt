/*
 * Webkul Software.
 *
 * Kotlin
 *
 * @author Webkul <support@webkul.com>
 * @category Webkul
 * @package com.jnana.delivery
 * @copyright 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html ASL Licence
 * @link https://store.webkul.com/license.html
 */

package com.yes.admin.network

import com.yes.admin.helpers.ConstantsHelper.ADD_BRAND
import com.yes.admin.helpers.ConstantsHelper.ADD_CATEGORY
import com.yes.admin.helpers.ConstantsHelper.ADD_CUSTOMER
import com.yes.admin.helpers.ConstantsHelper.ADD_MEASUREMENT
import com.yes.admin.helpers.ConstantsHelper.ADD_PRODUCT
import com.yes.admin.helpers.ConstantsHelper.ADD_SUB_CATEGORY
import com.yes.admin.helpers.ConstantsHelper.DELETE_BRAND
import com.yes.admin.helpers.ConstantsHelper.DELETE_CATEGORY
import com.yes.admin.helpers.ConstantsHelper.DELETE_MEASUREMENT
import com.yes.admin.helpers.ConstantsHelper.DELETE_PRODUCT
import com.yes.admin.helpers.ConstantsHelper.DELETE_SUB_CATEGORY
import com.yes.admin.helpers.ConstantsHelper.FORGET_PASSWORD
import com.yes.admin.helpers.ConstantsHelper.GET_ALL_COUNTS
import com.yes.admin.helpers.ConstantsHelper.GET_BRAND
import com.yes.admin.helpers.ConstantsHelper.GET_CATEGORY
import com.yes.admin.helpers.ConstantsHelper.GET_CUSTOMERS
import com.yes.admin.helpers.ConstantsHelper.GET_CUSTOMER_TYPE
import com.yes.admin.helpers.ConstantsHelper.GET_MEASUREMENT
import com.yes.admin.helpers.ConstantsHelper.GET_ORDERS
import com.yes.admin.helpers.ConstantsHelper.GET_PRODUCTS
import com.yes.admin.helpers.ConstantsHelper.GET_SUB_CATEGORY
import com.yes.admin.helpers.ConstantsHelper.MOBIKUL_CATALOG_HOME_PAGE_DATA
import com.yes.admin.helpers.ConstantsHelper.MOBIKUL_CUSTOMER_WEB_LOGIN
import com.yes.admin.helpers.ConstantsHelper.RESEND_OTP
import com.yes.admin.helpers.ConstantsHelper.RESET_PASSWORD
import com.yes.admin.helpers.ConstantsHelper.UPDATEDEVICETOKEN
import com.yes.admin.helpers.ConstantsHelper.UPDATE_BRAND
import com.yes.admin.helpers.ConstantsHelper.UPDATE_BRAND_STATUS
import com.yes.admin.helpers.ConstantsHelper.UPDATE_CATEGORY
import com.yes.admin.helpers.ConstantsHelper.UPDATE_CATEGORY_STATUS
import com.yes.admin.helpers.ConstantsHelper.UPDATE_MEASUREMENT
import com.yes.admin.helpers.ConstantsHelper.UPDATE_MEASUREMENT_STATUS
import com.yes.admin.helpers.ConstantsHelper.UPDATE_PRODUCT
import com.yes.admin.helpers.ConstantsHelper.UPDATE_PRODUCT_STATUS
import com.yes.admin.helpers.ConstantsHelper.UPDATE_SUB_CATEGORY
import com.yes.admin.helpers.ConstantsHelper.UPDATE_SUB_CATEGORY_STATUS
import com.yes.admin.helpers.ConstantsHelper.VERIFY_OTP
import com.yes.admin.models.BaseModel
import com.yes.admin.response.*
import com.yes.admin.session.Constants
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import retrofit2.http.Query

interface ApiDetails {
    @GET(MOBIKUL_CATALOG_HOME_PAGE_DATA)
    fun getHomePageData(@Query("eTag") eTag: String,
                        @Query("websiteId") websiteId: String,
                        @Query("storeId") storeId: String,
                        @Query("quoteId") quoteId: Int,
                        @Query("customerToken") customerToken: String,
                        @Query("currency") currency: String,
                        @Query("width") width: Int,
                        @Query("mFactor") mFactor: Float,
                        @Query("isFromUrl") isFromUrl: Int,
                        @Query("url") url: String):
            Observable<BaseModel>

    @FormUrlEncoded
    @POST(MOBIKUL_CUSTOMER_WEB_LOGIN)
    fun customerWebLoginNew(@Field("mobile") mobile: String,
                            @Field("password") password: String):
            Observable<LoginResponse>

    @FormUrlEncoded
    @POST(UPDATEDEVICETOKEN)
    fun uploadTokenData(
        @Field("device") device: String,
        @Field("device_id") device_id: String,
        @Field("token") token: String,
        @Field("shopper_id") shopper_id: Int,
    ): Observable<BaseModel>

    @FormUrlEncoded
    @POST(FORGET_PASSWORD)
    fun forgetPassword(@Field(Constants.ApiKeys.MOBILE) mobile: String):
            Observable<BaseModel>

    @FormUrlEncoded
    @POST(VERIFY_OTP)
    fun verifyOTP(@Field(Constants.ApiKeys.MOBILE) mobile: String,
                  @Field(Constants.ApiKeys.OTP) otp: String):
            Observable<BaseModel>

    @FormUrlEncoded
    @POST(RESET_PASSWORD)
    fun resetPassword(@Field(Constants.ApiKeys.MOBILE) mobile: String,
                      @Field(Constants.ApiKeys.NEW_PASSWORD) npass: String,
                      @Field(Constants.ApiKeys.CONFIRM_PASSWORD) cpass: String):
            Observable<BaseModel>

    @FormUrlEncoded
    @POST(RESEND_OTP)
    fun resendOTP(@Field(Constants.ApiKeys.MOBILE) mobile: String):
            Observable<BaseModel>

    @GET(GET_CUSTOMER_TYPE)
    fun getCUstomerType(): Observable<CustomerTypeResponse>

    @GET(GET_CATEGORY)
    fun getCategory(): Observable<CategoryResponse>

    @Multipart
    @POST(ADD_CATEGORY)
    fun addCategoryF(@Part("name") name: RequestBody,
                      @Part("customer_type") customerType: RequestBody,
                      @Part("information") description: RequestBody,
                      @Part("offer") offerinfo: RequestBody,
                      @Part files: MultipartBody.Part):
            Observable<AddCategoryResponse>

    @Multipart
    @POST(ADD_CATEGORY)
    fun addCategory(@Part("name") name: RequestBody,
                    @Part("customer_type") customerType: RequestBody,
                    @Part("information") description: RequestBody,
                    @Part("offer") offerinfo: RequestBody):
            Observable<AddCategoryResponse>

    @Multipart
    @POST(UPDATE_CATEGORY)
    fun updateCategoryF(@Part("id") id: RequestBody,
                       @Part("name") name: RequestBody,
                    @Part("customer_type") customerType: RequestBody,
                    @Part("information") description: RequestBody,
                    @Part("offer") offerinfo: RequestBody,
                    @Part files: MultipartBody.Part):
            Observable<AddCategoryResponse>

    @Multipart
    @POST(UPDATE_CATEGORY)
    fun updateCategory(@Part("id") id: RequestBody,
                       @Part("name") name: RequestBody,
                       @Part("customer_type") customerType: RequestBody,
                       @Part("information") description: RequestBody,
                       @Part("is_file") isFile: RequestBody,
                       @Part("offer") offerinfo: RequestBody):
            Observable<AddCategoryResponse>

    @FormUrlEncoded
    @POST(UPDATE_CATEGORY_STATUS)
    fun updateCategoryStatus(@Field("id") id: Int):
            Observable<AddCategoryResponse>

    @FormUrlEncoded
    @POST(DELETE_CATEGORY)
    fun deleteCategory(@Field("id") id: Int):
            Observable<BaseModel>

    @GET(GET_SUB_CATEGORY)
    fun getSubCategory(): Observable<SubCategoryResponse>

    @Multipart
    @POST(ADD_SUB_CATEGORY)
    fun addSubCategory(@Part("name") name: RequestBody,
                    @Part("category_id") categoryid: RequestBody,
                    @Part("information") description: RequestBody):
            Observable<AddSubCategoryResponse>

    @Multipart
    @POST(ADD_SUB_CATEGORY)
    fun addSubCategoryF(@Part("name") name: RequestBody,
                       @Part("category_id") categoryid: RequestBody,
                       @Part("information") description: RequestBody,
                       @Part files: MultipartBody.Part):
            Observable<AddSubCategoryResponse>

    @Multipart
    @POST(UPDATE_SUB_CATEGORY)
    fun updateSubCategory(@Part("id") id: RequestBody,
                          @Part("name") name: RequestBody,
                          @Part("category_id") categoryid: RequestBody,
                          @Part("information") description: RequestBody,
                          @Part("file_name") file_name: RequestBody,
                          @Part("is_file") isFile: RequestBody):
            Observable<AddSubCategoryResponse>

    @Multipart
    @POST(UPDATE_SUB_CATEGORY)
    fun updateSubCategoryF(@Part("id") id: RequestBody,
                           @Part("name") name: RequestBody,
                           @Part("category_id") categoryid: RequestBody,
                           @Part("information") description: RequestBody,
                          @Part files: MultipartBody.Part):
            Observable<AddSubCategoryResponse>

    @FormUrlEncoded
    @POST(UPDATE_SUB_CATEGORY_STATUS)
    fun updateSubCategoryStatus(@Field("id") id: Int):
            Observable<AddSubCategoryResponse>

    @FormUrlEncoded
    @POST(DELETE_SUB_CATEGORY)
    fun deleteSubCategory(@Field("id") id: Int):
            Observable<BaseModel>

    @GET(GET_BRAND)
    fun getBrand(): Observable<BrandResponse>

    @Multipart
    @POST(ADD_BRAND)
    fun addBrandF(@Part("name") name: RequestBody,
                     @Part("description") description: RequestBody,
                     @Part files: MultipartBody.Part):
            Observable<AddBrandResponse>

    @Multipart
    @POST(ADD_BRAND)
    fun addBrand(@Part("name") name: RequestBody,
                    @Part("description") description: RequestBody):
            Observable<AddBrandResponse>

    @Multipart
    @POST(UPDATE_BRAND)
    fun updateBrandF(@Part("id") id: RequestBody,
                        @Part("name") name: RequestBody,
                        @Part("description") description: RequestBody,
                        @Part files: MultipartBody.Part):
            Observable<AddBrandResponse>

    @Multipart
    @POST(UPDATE_BRAND)
    fun updateBrand(@Part("id") id: RequestBody,
                    @Part("name") name: RequestBody,
                    @Part("description") description: RequestBody):
            Observable<AddBrandResponse>

    @FormUrlEncoded
    @POST(UPDATE_BRAND_STATUS)
    fun updateBrandStatus(@Field("id") id: Int):
            Observable<AddBrandResponse>

    @FormUrlEncoded
    @POST(DELETE_BRAND)
    fun deleteBrand(@Field("id") id: Int):
            Observable<BaseModel>

    @GET(GET_MEASUREMENT)
    fun getMeasurement(): Observable<MeasurementResponse>

    @Multipart
    @POST(ADD_MEASUREMENT)
    fun addMeasurement(@Part("name") name: RequestBody):
            Observable<AddMeasurementResponse>

    @Multipart
    @POST(UPDATE_MEASUREMENT)
    fun updateMeasurement(@Part("id") id: RequestBody,
                       @Part("name") name: RequestBody):
            Observable<AddMeasurementResponse>

    @FormUrlEncoded
    @POST(UPDATE_MEASUREMENT_STATUS)
    fun updateMeasurementStatus(@Field("id") id: Int):
            Observable<AddMeasurementResponse>

    @FormUrlEncoded
    @POST(DELETE_MEASUREMENT)
    fun deleteMeasurement(@Field("id") id: Int):
            Observable<BaseModel>

    @GET(GET_PRODUCTS)
    fun getProducts(): Observable<ProductListResponse>

    @Multipart
    @POST(ADD_PRODUCT)
    fun addProductF(@Part("name") name: RequestBody,
                    @Part("category_id") category_id: RequestBody,
                    @Part("sub_category_id") sub_category_id: RequestBody,
                    @Part("description") description: RequestBody,
                    @Part("available_from_time") available_from_time: RequestBody,
                    @Part("available_to_time") available_to_time: RequestBody,
                    @Part("is_recommend") is_recommend: RequestBody,
                    @Part("home_display") home_display: RequestBody,
                    @Part("lowstock") lowstock: RequestBody,
                    @Part("hotsale") hotsale: RequestBody,
                    @Part("enquiry") enquiry: RequestBody,
                    @Part("quantities") quantities: RequestBody,
                    @Part files: MultipartBody.Part):
            Observable<AddProductResponse>

    @Multipart
    @POST(ADD_PRODUCT)
    fun addProduct(@Part("name") name: RequestBody,
                   @Part("category_id") category_id: RequestBody,
                   @Part("sub_category_id") sub_category_id: RequestBody,
                   @Part("description") description: RequestBody,
                   @Part("available_from_time") available_from_time: RequestBody,
                   @Part("available_to_time") available_to_time: RequestBody,
                   @Part("is_recommend") is_recommend: RequestBody,
                   @Part("home_display") home_display: RequestBody,
                   @Part("lowstock") lowstock: RequestBody,
                   @Part("hotsale") hotsale: RequestBody,
                   @Part("enquiry") enquiry: RequestBody,
                   @Part("quantities") quantities: RequestBody,):
            Observable<AddProductResponse>

    @Multipart
    @POST(UPDATE_PRODUCT)
    fun updateProductF(@Part("id") id: RequestBody,
                      @Part("name") name: RequestBody,
                       @Part files: MultipartBody.Part):
            Observable<AddProductResponse>

    @Multipart
    @POST(UPDATE_PRODUCT)
    fun updateProduct(@Part("id") id: RequestBody,
                          @Part("name") name: RequestBody):
            Observable<AddProductResponse>

    @FormUrlEncoded
    @POST(UPDATE_PRODUCT_STATUS)
    fun updateProductStatus(@Field("id") id: Int):
            Observable<ProductListResponse>

    @FormUrlEncoded
    @POST(DELETE_PRODUCT)
    fun deleteProduct(@Field("id") id: Int):
            Observable<BaseModel>

    @GET(GET_ORDERS)
    fun getOrders(): Observable<OrderListResponse>

    @GET(GET_CUSTOMERS)
    fun getCustomers(): Observable<CustomerListResponse>

    @Multipart
    @POST(ADD_CUSTOMER)
    fun addCustomer(@Part("name") name: RequestBody,
                    @Part("mobile") mobile: RequestBody,
                    @Part("email") email: RequestBody):
            Observable<BaseModel>

    @GET(GET_ALL_COUNTS)
    fun getAllCounts(): Observable<AllCountResponse>
}