package com.yes.admin.network

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.yes.admin.models.BaseModel
import com.yes.admin.response.*
import com.yes.admin.session.Constants
import com.yes.admin.session.SharedHelper
import com.yes.admin.session.TempSingleton
import com.yes.admin.utils.BaseUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import org.json.JSONObject


class ApiConnection private constructor() {
    companion object {
        private var apiConnection: ApiConnection? = null
        fun getInstance(): ApiConnection {
            if (apiConnection == null) {
                apiConnection = ApiConnection()
            }
            return apiConnection as ApiConnection
        }
    }

    fun uploadTokenData(context: Context):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).uploadTokenData("Android",SharedHelper(context).fcmToken,SharedHelper(context).token,SharedHelper(context).shop_id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun customerWebLogin(context: Context,mobile: String,password:String):LiveData<LoginResponse>{
        val apiResponse: MutableLiveData<LoginResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).customerWebLoginNew(mobile, password)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<LoginResponse>(context, false) {
                override fun onNext(responseModel: LoginResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = LoginResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun forgetPassword(context: Context,mobile: String):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).forgetPassword(mobile)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun verifyOTP(context: Context,mobile: String,otp:String):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).verifyOTP(mobile, otp)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun resetPassword(context: Context,mobile: String,npass:String,cpass:String):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).resetPassword(mobile, npass,cpass)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun resendOTP(context: Context,mobile: String):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).resendOTP(mobile)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getCUstomerType(context: Context):LiveData<CustomerTypeResponse>{
        val apiResponse: MutableLiveData<CustomerTypeResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getCUstomerType()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<CustomerTypeResponse>(context, false) {
                override fun onNext(responseModel: CustomerTypeResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = CustomerTypeResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getCategory(context: Context):LiveData<CategoryResponse>{
        val apiResponse: MutableLiveData<CategoryResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getCategory()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<CategoryResponse>(context, false) {
                override fun onNext(responseModel: CategoryResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = CategoryResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun addCategory(context: Context,name :String,customertype: Int,desc:String,offerinfo:String):LiveData<AddCategoryResponse>{
        val apiResponse: MutableLiveData<AddCategoryResponse> = MutableLiveData()
        if(TempSingleton.getInstance().multipartFileBody != null){
            ApiClient.getClient()!!.create(ApiDetails::class.java).addCategoryF(
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                customertype.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                offerinfo.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().multipartFileBody!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddCategoryResponse>(context, false) {
                    override fun onNext(responseModel: AddCategoryResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddCategoryResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
        else{
            ApiClient.getClient()!!.create(ApiDetails::class.java).addCategory(
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                customertype.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                offerinfo.toRequestBody("text/plain".toMediaTypeOrNull()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddCategoryResponse>(context, false) {
                    override fun onNext(responseModel: AddCategoryResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddCategoryResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
    }

    fun updateCategory(context: Context,id:Int,name :String,customertype: Int,desc:String,offerinfo:String):LiveData<AddCategoryResponse>{
        val apiResponse: MutableLiveData<AddCategoryResponse> = MutableLiveData()
        if(TempSingleton.getInstance().multipartFileBody != null) {
            ApiClient.getClient()!!.create(ApiDetails::class.java).updateCategoryF(
                id.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                customertype.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                offerinfo.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().multipartFileBody!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddCategoryResponse>(context, false) {
                    override fun onNext(responseModel: AddCategoryResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddCategoryResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
        else{
            ApiClient.getClient()!!.create(ApiDetails::class.java).updateCategory(
                id.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                customertype.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().isFile.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                offerinfo.toRequestBody("text/plain".toMediaTypeOrNull()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddCategoryResponse>(context, false) {
                    override fun onNext(responseModel: AddCategoryResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddCategoryResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
    }

    fun updateCategoryStatus(context: Context,id: Int):LiveData<AddCategoryResponse>{
        val apiResponse: MutableLiveData<AddCategoryResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).updateCategoryStatus(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<AddCategoryResponse>(context, false) {
                override fun onNext(responseModel: AddCategoryResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = AddCategoryResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun deleteCategory(context: Context,id: Int):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).deleteCategory(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getSubCategory(context: Context):LiveData<SubCategoryResponse>{
        val apiResponse: MutableLiveData<SubCategoryResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getSubCategory()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<SubCategoryResponse>(context, false) {
                override fun onNext(responseModel: SubCategoryResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = SubCategoryResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun addSubCategory(context: Context,name :String,cid: Int,desc:String):LiveData<AddSubCategoryResponse>{
        val apiResponse: MutableLiveData<AddSubCategoryResponse> = MutableLiveData()
        if(TempSingleton.getInstance().multipartFileBody !=  null){
            ApiClient.getClient()!!.create(ApiDetails::class.java).addSubCategoryF(
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                cid.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().multipartFileBody!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddSubCategoryResponse>(context, false) {
                    override fun onNext(responseModel: AddSubCategoryResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddSubCategoryResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
        else{
            ApiClient.getClient()!!.create(ApiDetails::class.java).addSubCategory(
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                cid.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddSubCategoryResponse>(context, false) {
                    override fun onNext(responseModel: AddSubCategoryResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddSubCategoryResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
    }

    fun updateSubCategory(context: Context,id:Int,name :String,cid: Int,desc:String):LiveData<AddSubCategoryResponse>{
        val apiResponse: MutableLiveData<AddSubCategoryResponse> = MutableLiveData()
        if(TempSingleton.getInstance().multipartFileBody != null){
            ApiClient.getClient()!!.create(ApiDetails::class.java).updateSubCategoryF(
                id.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                cid.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().multipartFileBody!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddSubCategoryResponse>(context, false) {
                    override fun onNext(responseModel: AddSubCategoryResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddSubCategoryResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
        else{
            ApiClient.getClient()!!.create(ApiDetails::class.java).updateSubCategory(
                id.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                cid.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                "".toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().isFile.toString().toRequestBody("text/plain".toMediaTypeOrNull()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddSubCategoryResponse>(context, false) {
                    override fun onNext(responseModel: AddSubCategoryResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddSubCategoryResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
    }

    fun updateSubCategoryStatus(context: Context,id: Int):LiveData<AddSubCategoryResponse>{
        val apiResponse: MutableLiveData<AddSubCategoryResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).updateSubCategoryStatus(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<AddSubCategoryResponse>(context, false) {
                override fun onNext(responseModel: AddSubCategoryResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = AddSubCategoryResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun deleteSubCategory(context: Context,id: Int):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).deleteSubCategory(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getBrand(context: Context):LiveData<BrandResponse>{
        val apiResponse: MutableLiveData<BrandResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getBrand()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BrandResponse>(context, false) {
                override fun onNext(responseModel: BrandResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BrandResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun addBrand(context: Context,name :String,desc:String):LiveData<AddBrandResponse>{
        val apiResponse: MutableLiveData<AddBrandResponse> = MutableLiveData()
        if(TempSingleton.getInstance().multipartFileBody != null){
            ApiClient.getClient()!!.create(ApiDetails::class.java).addBrandF(
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().multipartFileBody!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddBrandResponse>(context, false) {
                    override fun onNext(responseModel: AddBrandResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddBrandResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
        else{
            ApiClient.getClient()!!.create(ApiDetails::class.java).addBrand(
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddBrandResponse>(context, false) {
                    override fun onNext(responseModel: AddBrandResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddBrandResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
    }

    fun updateBrand(context: Context,id:Int,name :String,desc:String):LiveData<AddBrandResponse>{
        val apiResponse: MutableLiveData<AddBrandResponse> = MutableLiveData()
        if(TempSingleton.getInstance().multipartFileBody != null) {
            ApiClient.getClient()!!.create(ApiDetails::class.java).updateBrandF(
                id.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().multipartFileBody!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddBrandResponse>(context, false) {
                    override fun onNext(responseModel: AddBrandResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddBrandResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
        else{
            ApiClient.getClient()!!.create(ApiDetails::class.java).updateBrand(
                id.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                desc.toRequestBody("text/plain".toMediaTypeOrNull()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddBrandResponse>(context, false) {
                    override fun onNext(responseModel: AddBrandResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddBrandResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
    }

    fun updateBrandStatus(context: Context,id: Int):LiveData<AddBrandResponse>{
        val apiResponse: MutableLiveData<AddBrandResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).updateBrandStatus(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<AddBrandResponse>(context, false) {
                override fun onNext(responseModel: AddBrandResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = AddBrandResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun deleteBrand(context: Context,id: Int):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).deleteBrand(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getMeasurement(context: Context):LiveData<MeasurementResponse>{
        val apiResponse: MutableLiveData<MeasurementResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getMeasurement()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<MeasurementResponse>(context, false) {
                override fun onNext(responseModel: MeasurementResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = MeasurementResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun addMeasurement(context: Context,name :String):LiveData<AddMeasurementResponse>{
        val apiResponse: MutableLiveData<AddMeasurementResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).addMeasurement(
            name.toRequestBody("text/plain".toMediaTypeOrNull()))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<AddMeasurementResponse>(context, false) {
                    override fun onNext(responseModel: AddMeasurementResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddMeasurementResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
        return apiResponse
    }

    fun updateMeasurement(context: Context,id:Int,name :String):LiveData<AddMeasurementResponse>{
        val apiResponse: MutableLiveData<AddMeasurementResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).updateMeasurement(
            id.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
            name.toRequestBody("text/plain".toMediaTypeOrNull()))
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<AddMeasurementResponse>(context, false) {
                    override fun onNext(responseModel: AddMeasurementResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddMeasurementResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
        return apiResponse
    }

    fun updateMeasurementStatus(context: Context,id: Int):LiveData<AddMeasurementResponse>{
        val apiResponse: MutableLiveData<AddMeasurementResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).updateMeasurementStatus(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<AddMeasurementResponse>(context, false) {
                override fun onNext(responseModel: AddMeasurementResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = AddMeasurementResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun deleteMeasurement(context: Context,id: Int):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).deleteMeasurement(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getProducts(context: Context):LiveData<ProductListResponse>{
        val apiResponse: MutableLiveData<ProductListResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getProducts()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<ProductListResponse>(context, false) {
                override fun onNext(responseModel: ProductListResponse) {
                    super.onNext(responseModel)
                    if(responseModel.error){
                        apiResponse.value = responseModel
                    }
                    else{
                        val jsonObject = JSONObject(Gson().toJson(responseModel))
                        val data = jsonObject.getJSONArray(Constants.ApiKeys.DATA)
                        jsonObject.put(Constants.ApiKeys.DATA,productScan(data))
                        Log.d("jchbv",""+jsonObject)
                        val responseModelNew: ProductListResponse = Gson().fromJson(jsonObject.toString(), ProductListResponse::class.java)
                        apiResponse.value = responseModelNew
                    }
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = ProductListResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun addProduct(context: Context):LiveData<AddProductResponse>{
        val apiResponse: MutableLiveData<AddProductResponse> = MutableLiveData()
        if(TempSingleton.getInstance().multipartFileBody != null){
            ApiClient.getClient()!!.create(ApiDetails::class.java).addProductF(
                TempSingleton.getInstance().pname.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().cid.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().scid.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().available_from_time.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().available_to_time.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().is_recommend.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().home_display.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().lowstock.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().hotsale.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().enquiry.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().quantities.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().multipartFileBody!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddProductResponse>(context, false) {
                    override fun onNext(responseModel: AddProductResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddProductResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
        else{
            ApiClient.getClient()!!.create(ApiDetails::class.java).addProduct(
                TempSingleton.getInstance().pname.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().cid.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().scid.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().desc.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().available_from_time.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().available_to_time.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().is_recommend.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().home_display.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().lowstock.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().hotsale.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().enquiry.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().quantities.toString().toRequestBody("text/plain".toMediaTypeOrNull()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddProductResponse>(context, false) {
                    override fun onNext(responseModel: AddProductResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddProductResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
    }

    fun updateProduct(context: Context,id:Int,name :String,desc:String):LiveData<AddProductResponse>{
        val apiResponse: MutableLiveData<AddProductResponse> = MutableLiveData()
        if(TempSingleton.getInstance().multipartFileBody != null) {
            ApiClient.getClient()!!.create(ApiDetails::class.java).updateProductF(
                id.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                TempSingleton.getInstance().multipartFileBody!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddProductResponse>(context, false) {
                    override fun onNext(responseModel: AddProductResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddProductResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
        else{
            ApiClient.getClient()!!.create(ApiDetails::class.java).updateProduct(
                id.toString().toRequestBody("text/plain".toMediaTypeOrNull()),
                name.toRequestBody("text/plain".toMediaTypeOrNull()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<AddProductResponse>(context, false) {
                    override fun onNext(responseModel: AddProductResponse) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = AddProductResponse()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
        }
    }

    fun updateProductStatus(context: Context,id: Int):LiveData<ProductListResponse>{
        val apiResponse: MutableLiveData<ProductListResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).updateProductStatus(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<ProductListResponse>(context, false) {
                override fun onNext(responseModel: ProductListResponse) {
                    super.onNext(responseModel)
                    if(responseModel.error){
                        apiResponse.value = responseModel
                    }
                    else{
                        val jsonObject = JSONObject(Gson().toJson(responseModel))
                        val data = jsonObject.getJSONArray(Constants.ApiKeys.DATA)
                        jsonObject.put(Constants.ApiKeys.DATA,productScan(data))
                        val responseModelNew: ProductListResponse = Gson().fromJson(jsonObject.toString(), ProductListResponse::class.java)
                        apiResponse.value = responseModelNew
                    }                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = ProductListResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun deleteProduct(context: Context,id: Int):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).deleteProduct(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                override fun onNext(responseModel: BaseModel) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = BaseModel()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun getOrders(context: Context):LiveData<OrderListResponse>{
        val apiResponse: MutableLiveData<OrderListResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getOrders()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<OrderListResponse>(context, false) {
                override fun onNext(responseModel: OrderListResponse) {
                    super.onNext(responseModel)
                    if(responseModel.error){
                        apiResponse.value = responseModel
                    }
                    else{
                        val jsonObject = JSONObject(Gson().toJson(responseModel))
                        val data = jsonObject.getJSONArray(Constants.ApiKeys.DATA)
                        for(i in 0 until data.length()){
                            val items =  data.getJSONObject(i).getJSONArray(Constants.ApiKeys.ITEMS)
                            for(j in 0 until items.length()){
                                val product = items.getJSONObject(j).getJSONObject(Constants.ApiKeys.PRODUCT)
                                items.getJSONObject(j).put(Constants.ApiKeys.PRODUCT,productScan(JSONArray().put(0,product))[0])
                            }
                        }
                        val responseModelNew: OrderListResponse = Gson().fromJson(jsonObject.toString(), OrderListResponse::class.java)
                        apiResponse.value = responseModelNew
                    }
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = OrderListResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun productScan(jsonArray: JSONArray): JSONArray {
        for (i in 0 until jsonArray.length()) {
            if(jsonArray.getJSONObject(i).has(Constants.ApiKeys.FILES)){
                val files = jsonArray.getJSONObject(i).get(Constants.ApiKeys.FILES)
                if(!BaseUtils.isArray(files)){
                    val jArray = JSONArray()
                    jArray.put(files)
                    jsonArray.getJSONObject(i).put("filesNew",jArray)
                }
                else{
                    jsonArray.getJSONObject(i).put("filesNew",files)
                }
            }
            if(jsonArray.getJSONObject(i).has(Constants.ApiKeys.ORIGINAL_FILES)){
                val oFiles = jsonArray.getJSONObject(i).get(Constants.ApiKeys.ORIGINAL_FILES)
                if(!BaseUtils.isArray(oFiles)){
                    jsonArray.getJSONObject(i).put(Constants.ApiKeys.ORIGINAL_FILES, JSONArray(oFiles))
                }
            }
        }
        return jsonArray
    }

    fun getCustomers(context: Context):LiveData<CustomerListResponse>{
        val apiResponse: MutableLiveData<CustomerListResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getCustomers()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<CustomerListResponse>(context, false) {
                override fun onNext(responseModel: CustomerListResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = CustomerListResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }

    fun addCustomer(context: Context,name :String,mobile: String,email:String):LiveData<BaseModel>{
        val apiResponse: MutableLiveData<BaseModel> = MutableLiveData()
            ApiClient.getClient()!!.create(ApiDetails::class.java).addCustomer(
                name.toRequestBody("text/plain".toMediaTypeOrNull()),
                mobile.toRequestBody("text/plain".toMediaTypeOrNull()),
                email.toRequestBody("text/plain".toMediaTypeOrNull()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : ApiResponseCallback<BaseModel>(context, false) {
                    override fun onNext(responseModel: BaseModel) {
                        super.onNext(responseModel)
                        apiResponse.value = responseModel
                    }
                    override fun onError(e: Throwable) {
                        super.onError(e)
                        val response = BaseModel()
                        response.error = true
                        response.status = e.message.toString()
                        apiResponse.value = response
                    }
                })
            return apiResponse
    }

    fun getAllCounts(context: Context):LiveData<AllCountResponse>{
        val apiResponse: MutableLiveData<AllCountResponse> = MutableLiveData()
        ApiClient.getClient()!!.create(ApiDetails::class.java).getAllCounts()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object : ApiResponseCallback<AllCountResponse>(context, false) {
                override fun onNext(responseModel: AllCountResponse) {
                    super.onNext(responseModel)
                    apiResponse.value = responseModel
                }
                override fun onError(e: Throwable) {
                    super.onError(e)
                    val response = AllCountResponse()
                    response.error = true
                    response.status = e.message.toString()
                    apiResponse.value = response
                }
            })
        return apiResponse
    }
}