package com.yes.admin.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.admin.R
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.databinding.CardOrderBinding
import com.yes.admin.models.*
import com.yes.admin.session.Constants
import com.yes.admin.utils.BaseUtils
import com.yes.admin.utils.UiUtils

class OrdersAdapter(
    var activity: DashBoardActivity,
    var context: Context,
    var list: ArrayList<OrderListModel>) : RecyclerView.Adapter<OrdersAdapter.ViewHolder>() {
    private lateinit var dashBoardActivity: DashBoardActivity
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardOrderBinding = CardOrderBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        dashBoardActivity = activity
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_order,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val list = list[position]
        holder.binding.cInfo.text = list.customer!!.name+"\n"+list.customer!!.mobile
        holder.binding.oNo.text = list.invoice
        holder.binding.oDate.text = BaseUtils.getFormattedDate(list.created_at!!,Constants.ApiKeys.TIME_INPUT_FORMAT,"MMM dd, yyyy, hh:mm aaa")
        holder.binding.oValue.text = list.grand_total

        holder.binding.view.setOnClickListener {
        }

        holder.binding.edit.setOnClickListener {

        }
        when (list.status) {
            1 -> {
                UiUtils.textViewBgTint(holder.binding.oStatus,"#007BFF",null)
                holder.binding.oStatus.text = "Ordered"
            }
            2 -> {

            }
            3 -> {

            }
            4 -> {
                UiUtils.textViewBgTint(holder.binding.oStatus,"#007BFF",null)
                holder.binding.oStatus.text = "Order Accepted"
            }
            5 -> {
                UiUtils.textViewBgTint(holder.binding.oStatus,"#007BFF",null)
                holder.binding.oStatus.text = "Order Assigned"
            }
            6 -> {
                UiUtils.textViewBgTint(holder.binding.oStatus,"#81BA00",null)
                holder.binding.oStatus.text = "Completed"
            }
        }
        when {
            list.order_type.toString().toInt() == 1 -> {
                val text = "${context.getString(R.string.upi)}"
                holder.binding.pStatus.text = text
                UiUtils.textViewBgTint(holder.binding.pStatus,"#81BA00",null)
            }
            list.order_type.toString().toInt() == 2 -> {
                val text = "${context.getString(R.string.cash_on_delivery)}"
                holder.binding.pStatus.text = text
                UiUtils.textViewBgTint(holder.binding.pStatus,"#81BA00",null)
            }
            list.order_type.toString().toInt() == 3 -> {
                val text = "${context.getString(R.string.card)}"
                holder.binding.pStatus.text = text
                UiUtils.textViewBgTint(holder.binding.pStatus,"#81BA00",null)
            }
            list.order_type.toString().toInt() == 4 -> {
                val text = "${context.getString(R.string.wallet)}"
                holder.binding.pStatus.text = text
                UiUtils.textViewBgTint(holder.binding.pStatus,"#81BA00",null)
            }
            list.order_type.toString().toInt() == 5 -> {
                val text = "${context.getString(R.string.wallet)}${"+"}"
                holder.binding.pStatus.text = text
                UiUtils.textViewBgTint(holder.binding.pStatus,"#81BA00",null)
            }
        }
    }
}