package com.yes.admin.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.yes.admin.fragment.*
import com.yes.admin.models.ProductModel
import com.yes.admin.session.Constants

private const val NUM_TABS = 4
class ViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle, productModel: ProductModel?) : FragmentStateAdapter(fragmentManager, lifecycle) {
    val productModel = productModel
    override fun getItemCount(): Int {
        return NUM_TABS
    }

    override fun createFragment(position: Int): Fragment {
        if(productModel != null){
            when (position) {
                0 ->{
                    return  ProductGeneralFragment().apply {
                        arguments = Bundle().apply {
                            putString(Constants.IntentKeys.PRODUCT, productModel.name)
                            putInt(Constants.IntentKeys.CID, productModel.category_id!!)
                            putInt(Constants.IntentKeys.S_C_ID, productModel.sub_category_id!!)
                            putInt(Constants.IntentKeys.BID, productModel.brand_id!!)
                            putString(Constants.IntentKeys.DESCRIPTION, productModel.description)
                        }
                    }
                }
                1 -> return  ProductUnitFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(Constants.IntentKeys.QUANTITIES, productModel.quantities)
                    }
                }
                2 -> return  ProductPhotoFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(Constants.IntentKeys.FILES, productModel.filesNew)
                    }
                }
                3 -> return ProductAttributeFragment()
            }
        }
        else{
            when (position) {
                0 ->{
                    return  ProductGeneralFragment()
                }
                1 -> return ProductUnitFragment()
                2 -> return ProductPhotoFragment()
                3 -> return ProductAttributeFragment()
            }
        }

        return Fragment()
    }
}