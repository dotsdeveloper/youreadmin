package com.yes.admin.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.admin.R
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.databinding.CardProfileBinding
import com.yes.admin.models.*
import com.yes.admin.utils.UiUtils

class ProfileAdapter(
    var activity: DashBoardActivity,
    var context: Context,
    var list: ArrayList<CustomerModel>) : RecyclerView.Adapter<ProfileAdapter.ViewHolder>() {
    private lateinit var dashBoardActivity: DashBoardActivity
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardProfileBinding = CardProfileBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        dashBoardActivity = activity
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_profile,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val list = list[position]
        holder.binding.cname.text = list.user!!.name+"\n"+list.user!!.mobile+"\n"+list.user!!.email+"\n"+list.referral_code

        if(list.order_history != null && list.balance != null && list.reward_point != null){
            val li = "${"Count : "+list.order_history!!.user_count}${"\n"}${"Value : ₹"+list.order_history!!.order_total}${"\n"}${"Credit Amount : "+list.balance}${"\n"}${"Rewards : "+list.reward_point}"
            holder.binding.name.text = li
        }
        else{
            holder.binding.name.text = "N/A"
        }


        holder.binding.view.setOnClickListener {
        }

        holder.binding.edit.setOnClickListener {

        }

        if(list.status == 1){
            UiUtils.viewBgTint(holder.binding.status,null, R.color.green)
        }
        else{
            UiUtils.viewBgTint(holder.binding.status,null, R.color.red)
        }
    }
}