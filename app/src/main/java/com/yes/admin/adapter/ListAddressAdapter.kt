package com.yes.admin.adapter
import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.recyclerview.widget.RecyclerView
import com.yes.admin.R
import com.yes.admin.activity.CheckoutActivity
import com.yes.admin.databinding.CardListaddressBinding
import com.yes.admin.utils.UiUtils
import com.yes.youreagency.responses.AddressData
import kotlin.collections.ArrayList

class ListAddressAdapter(
    var view: Int,
    var checkoutActivity: CheckoutActivity?,
    var context: Context,
    var list: ArrayList<AddressData>?
) :
    RecyclerView.Adapter<ListAddressAdapter.HomeHeaderViewHolder>() {
    var selectedPosition = -1

    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardListaddressBinding = CardListaddressBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_listaddress,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {

        if(view == 1){
            if(selectedPosition == position){
                holder.binding.name.text = list!![position].location_name
                holder.binding.landmark.text = list!![position].land_mark
                holder.binding.location.text = list!![position].address
                holder.binding.pincode.text = list!![position].pincode
                UiUtils.textViewBgTint(holder.binding.deliveryhere,checkoutActivity!!.sharedHelper.primarycolor,null)
                holder.binding.deliveryhere.setTextColor(Color.WHITE)
            }
            else {
                    holder.binding.name.text = list!![position].location_name
                    holder.binding.landmark.text = list!![position].land_mark
                    holder.binding.location.text = list!![position].address
                    holder.binding.pincode.text = list!![position].pincode
                    holder.binding.deliveryhere.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.WHITE, BlendModeCompat.SRC_ATOP)
                    UiUtils.textViewTextColor(holder.binding.deliveryhere,checkoutActivity!!.sharedHelper.primarycolor,null)
                    holder.binding.deliveryhere.setOnClickListener {
                        checkoutActivity!!.deliveryid = list!![position].id.toString()
                        checkoutActivity!!.locationname = list!![position].location_name.toString()
                        checkoutActivity!!.flat = list!![position].location_lat!!.toDouble()
                        checkoutActivity!!.flng = list!![position].location_long!!.toDouble()
                        checkoutActivity!!.spincode = list!![position].pincode.toString()
                        checkoutActivity!!.address = list!![position].address.toString()
                        checkoutActivity!!.landmark = list!![position].land_mark.toString()
                        checkoutActivity!!.isaddaddress = false
                        checkoutActivity!!.isselectedaddress = true
                        selectedPosition = position
                        notifyDataSetChanged()
                    }
            }

           /* if(position == (list!!.size-1)){
                val observer: ViewTreeObserver = holder.binding.card.getViewTreeObserver()
                observer.addOnGlobalLayoutListener {
                    val params: ViewGroup.LayoutParams = checkoutActivity!!.binding.recyclerAddress.getLayoutParams()
                    params.height = holder.binding.card.getHeight()*2
                    checkoutActivity!!.binding.recyclerAddress.setLayoutParams(params)
                }
            }*/
        }
        else if(view == 0){
            holder.binding.name.text = list!![position].location_name
            holder.binding.landmark.text = list!![position].land_mark
            holder.binding.location.text = list!![position].address
            holder.binding.pincode.text = list!![position].pincode
            holder.binding.deliveryhere.visibility = View.GONE
        }

    }
}