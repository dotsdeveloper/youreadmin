package com.yes.admin.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.admin.R
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.databinding.CardCategoryBinding
import com.yes.admin.interfaces.OnClickListener
import com.yes.admin.models.*
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils

class ProductAdapter(
    var activity: DashBoardActivity,
    var context: Context,
    var productlist: ArrayList<ProductModel>
) :
    RecyclerView.Adapter<ProductAdapter.ViewHolder>() {
    private lateinit var dashBoardActivity: DashBoardActivity
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardCategoryBinding = CardCategoryBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        dashBoardActivity = activity
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_category,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return when {
            productlist.isNotEmpty() -> {
                productlist.size
            }
            else -> {
                0
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        if(productlist.isNotEmpty()){
            val list = productlist[position]
            holder.binding.img.visibility = View.VISIBLE
            holder.binding.cname.visibility = View.GONE
            holder.binding.name.text = list.name

            if(list.filesNew.isNotEmpty()){
                UiUtils.loadImage(holder.binding.img, list.filesNew[0])
            }
            else{
                UiUtils.loadDefaultImage(holder.binding.img)
            }

            if(list.status == 1){
                UiUtils.viewBgTint(holder.binding.status,null, R.color.green)
            }
            else{
                UiUtils.viewBgTint(holder.binding.status,null, R.color.red)
            }

            holder.binding.view.setOnClickListener {
                DialogUtils.showViewProductDialog(context,list)
            }

            holder.binding.edit.setOnClickListener {
                DialogUtils.showAddProductDialog(context,
                    dashBoardActivity,
                    dashBoardActivity.supportFragmentManager,
                    dashBoardActivity.lifecycle,
                    list,
                    object : OnClickListener {
                        override fun onClickItem(
                            data1: CategoryListModel,
                            data2: SubCategoryListModel,
                            data3: BrandModel,
                            data4: ProductUnitModel,
                        ) {

                        }

                        override fun onClickItem() {
                        }
                    })
            }
        }
    }
}