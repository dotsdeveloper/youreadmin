package com.yes.admin.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yes.admin.R
import com.yes.admin.activity.DashBoardActivity
import com.yes.admin.databinding.CardCategoryBinding
import com.yes.admin.interfaces.OnClickListener
import com.yes.admin.models.BrandModel
import com.yes.admin.models.CategoryListModel
import com.yes.admin.models.ProductUnitModel
import com.yes.admin.models.SubCategoryListModel
import com.yes.admin.utils.BaseUtils
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils

class CategoryAdapter(
    var activity: DashBoardActivity,
    var context: Context,
    var categorylist: ArrayList<CategoryListModel>,
    var subcategorylist: ArrayList<SubCategoryListModel>,
    var brandlist: ArrayList<BrandModel>,
    var productunitlist: ArrayList<ProductUnitModel>
) :
    RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    private lateinit var dashBoardActivity: DashBoardActivity
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardCategoryBinding = CardCategoryBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       dashBoardActivity = activity
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_category,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return when {
            categorylist.isNotEmpty() -> {
                categorylist.size
            }
            subcategorylist.isNotEmpty() -> {
                subcategorylist.size
            }
            brandlist.isNotEmpty() -> {
                brandlist.size
            }
            productunitlist.isNotEmpty() -> {
                productunitlist.size
            }
            else -> {
                0
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        if(categorylist.isNotEmpty()){
            val list = categorylist[position]
            holder.binding.img.visibility = View.VISIBLE
            holder.binding.cname.visibility = View.GONE
            holder.binding.name.text = list.name
            holder.binding.view.setOnClickListener {
                DialogUtils.showViewCategoryDialog(context,list,null,null,null)
            }

            holder.binding.edit.setOnClickListener {
                DialogUtils.showAddCategoryDialog(context,
                    dashBoardActivity,
                    true,
                    false,
                    false,
                    false,
                    list,
                    null,
                    null,
                    null,
                    dashBoardActivity,
                    object : OnClickListener {
                        override fun onClickItem(data1: CategoryListModel,data2: SubCategoryListModel,data3: BrandModel,data4: ProductUnitModel) {
                            categorylist.removeAt(position)
                            categorylist.add(position,data1)
                            notifyItemChanged(position)
                            dashBoardActivity.load()
                        }

                        override fun onClickItem() {
                        }
                    })
            }

            if(BaseUtils.nullCheckerStr(list.file).isNotEmpty()){
                UiUtils.loadImage(holder.binding.img,list.file)
            }
            else{
                UiUtils.loadDefaultImage(holder.binding.img)
            }

            if(list.status == 1){
                UiUtils.viewBgTint(holder.binding.status,null,R.color.green)
            }
            else{
                UiUtils.viewBgTint(holder.binding.status,null,R.color.red)
            }
        }
        else if(subcategorylist.isNotEmpty()){
            val list = subcategorylist[position]
            holder.binding.img.visibility = View.GONE
            holder.binding.cname.visibility = View.VISIBLE
            holder.binding.name.text = list.name
            holder.binding.cname.text =list.category!!.name
            holder.binding.view.setOnClickListener {
                DialogUtils.showViewCategoryDialog(context,null,list,null,null)
            }

            holder.binding.edit.setOnClickListener {
                DialogUtils.showAddCategoryDialog(context,
                    dashBoardActivity,
                    false,
                    true,
                    false,
                    false,
                    null,
                    list,
                    null,
                    null,
                    dashBoardActivity,
                    object : OnClickListener {
                        override fun onClickItem(data1: CategoryListModel,data2: SubCategoryListModel,data3: BrandModel,data4: ProductUnitModel) {
                            subcategorylist.removeAt(position)
                            subcategorylist.add(position,data2)
                            notifyItemChanged(position)
                            dashBoardActivity.load()
                        }
                        override fun onClickItem() {
                        }
                    })
            }

            if(BaseUtils.nullCheckerStr(list.file).isNotEmpty()){
                UiUtils.loadImage(holder.binding.img,list.file)
            }
            else{
                UiUtils.loadDefaultImage(holder.binding.img)
            }

            if(list.status == 1){
                UiUtils.viewBgTint(holder.binding.status,null,R.color.green)
            }
            else{
                UiUtils.viewBgTint(holder.binding.status,null,R.color.red)
            }
        }
        else if(brandlist.isNotEmpty()){
            val list = brandlist[position]
            holder.binding.img.visibility = View.VISIBLE
            holder.binding.cname.visibility = View.GONE
            holder.binding.name.visibility = View.VISIBLE
            holder.binding.name.text = list.name

            if(BaseUtils.nullCheckerStr(list.file).isNotEmpty()){
                UiUtils.loadImage(holder.binding.img,list.file)
            }
            else{
                UiUtils.loadDefaultImage(holder.binding.img)
            }

            if(list.status == 1){
                UiUtils.viewBgTint(holder.binding.status,null,R.color.green)
            }
            else{
                UiUtils.viewBgTint(holder.binding.status,null,R.color.red)
            }

            holder.binding.view.setOnClickListener {
                DialogUtils.showViewCategoryDialog(context,null,null,list,null)
            }


            holder.binding.edit.setOnClickListener {
                DialogUtils.showAddCategoryDialog(context,
                    dashBoardActivity,
                    false,
                    false,
                    true,
                    false,
                    null,
                    null,
                    list,
                    null,
                    dashBoardActivity,
                    object : OnClickListener {
                        override fun onClickItem(data1: CategoryListModel,data2: SubCategoryListModel,data3: BrandModel,data4: ProductUnitModel) {
                            brandlist.removeAt(position)
                            brandlist.add(position,data3)
                            notifyItemChanged(position)
                            dashBoardActivity.load()
                        }

                        override fun onClickItem() {
                        }
                    })
            }


        }
        else if(productunitlist.isNotEmpty()){
            val list = productunitlist[position]
            holder.binding.img.visibility = View.VISIBLE
            holder.binding.cname.visibility = View.GONE
            holder.binding.name.visibility = View.VISIBLE
            holder.binding.name.text = list.name

            if(list.status == 1){
                UiUtils.viewBgTint(holder.binding.status,null,R.color.green)
            }
            else{
                UiUtils.viewBgTint(holder.binding.status,null,R.color.red)
            }

            holder.binding.view.setOnClickListener {
                DialogUtils.showViewCategoryDialog(context,null,null,null,list)
            }


            holder.binding.edit.setOnClickListener {
                DialogUtils.showAddCategoryDialog(context,
                    dashBoardActivity,
                    false,
                    false,
                    false,
                    true,
                    null,
                    null,
                    null,
                    list,
                    dashBoardActivity,
                    object : OnClickListener {
                        override fun onClickItem(data1: CategoryListModel,data2: SubCategoryListModel,data3: BrandModel,data4: ProductUnitModel) {
                            productunitlist.removeAt(position)
                            productunitlist.add(position,data4)
                            notifyItemChanged(position)
                            dashBoardActivity.load()
                        }

                        override fun onClickItem() {
                        }
                    })
            }


        }
    }
}