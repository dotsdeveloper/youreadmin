package com.yes.admin.adapter

import android.content.Context
import android.view.*
import androidx.recyclerview.widget.RecyclerView
import kotlin.collections.ArrayList
import com.yes.admin.R
import com.yes.admin.databinding.CardProductQuantityBinding
import com.yes.admin.models.QuantitiesModel
import com.yes.admin.session.Constants
import com.yes.admin.utils.UiUtils

class ProductQuantityAdapter(
    var context: Context,
    var list: ArrayList<QuantitiesModel>,
) :
    RecyclerView.Adapter<ProductQuantityAdapter.ViewHolder>() {
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardProductQuantityBinding = CardProductQuantityBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_product_quantity,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val kg = "${list[position].quantity} ${list[position].product_unit!!.name}"
        holder.binding.qty.text = kg
        holder.binding.sno.text = (position+1).toString()
        val stock = "${list[position].stock} ${list[position].product_unit!!.name}"
        holder.binding.stock.text = stock
        val mstock = "${list[position].minimum_stock} ${list[position].product_unit!!.name}"
        holder.binding.minStock.text = mstock

        if(UiUtils.formattedValues(list[position].offer_price) == Constants.IntentKeys.AMOUNT_DUMMY){
            holder.binding.price1.visibility = View.VISIBLE
            val price1 = "${Constants.Common.CURRENCY}${list[position].amount}"
            holder.binding.price1.text = price1
            UiUtils.textViewTextColor(holder.binding.price1,null,R.color.black)
            holder.binding.price2.visibility = View.GONE
        }
        else {
            holder.binding.price1.visibility = View.VISIBLE
            holder.binding.price2.visibility = View.VISIBLE
            val price1 = "${Constants.Common.CURRENCY}${list[position].offer_price}"
            val price2 = "${Constants.Common.CURRENCY}${list[position].amount}"
            holder.binding.price1.text = price1
            UiUtils.textViewTextColor(holder.binding.price1,null,R.color.black)
            holder.binding.price2.text = price2
            UiUtils.textViewTextColor(holder.binding.price2,null,R.color.red)
            UiUtils.textviewCustomDrawable(holder.binding.price2,R.drawable.line_strike)
        }
    }
}