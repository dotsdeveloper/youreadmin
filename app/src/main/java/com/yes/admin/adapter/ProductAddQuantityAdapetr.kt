package com.yes.admin.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yes.admin.R
import com.yes.admin.databinding.CardAddProductUnitBinding
import com.yes.admin.fragment.ProductUnitFragment
import org.json.JSONArray


class ProductAddQuantityAdapetr(
    var context: Context,
    var productUnitFragment: ProductUnitFragment,
    var unitlist: ArrayList<String>,
    var jsonArray: JSONArray
) :
    RecyclerView.Adapter<ProductAddQuantityAdapetr.ViewHolder>() {
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardAddProductUnitBinding = CardAddProductUnitBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_add_product_unit,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return jsonArray.length()
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val dataAdapter1: ArrayAdapter<String> = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, unitlist)
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        holder.binding.unitSpinner.adapter = dataAdapter1
        holder.binding.unitSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                pos: Int,
                id: Long,
            ) {
                val item: String = parent.getItemAtPosition(pos).toString()
                val uid = if(pos == 0){
                    0
                } else{
                    productUnitFragment.sharedHelper.productUnitList[pos-1].id!!
                }
                jsonArray.getJSONObject(position).put("unit_id",uid)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // sometimes you need nothing here
            }
        }
        holder.binding.unitSpinner.setSelection(0)

        if(jsonArray.length() > 0){
            holder.binding.qty.setText(jsonArray.getJSONObject(position).get("quantity").toString())
            holder.binding.price.setText(jsonArray.getJSONObject(position).get("amount").toString())
            holder.binding.offerPrice.setText(jsonArray.getJSONObject(position).get("offer_price").toString())
            for ((index, value) in productUnitFragment.sharedHelper.productUnitList.withIndex()) {
                val id = jsonArray.getJSONObject(position).get("unit_id")
                if(value.id == id){
                    holder.binding.unitSpinner.setSelection(index+1)
                }
            }
        }

        holder.binding.qty.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                jsonArray.getJSONObject(position).put("quantity",holder.binding.qty.text)
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })

        holder.binding.price.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                jsonArray.getJSONObject(position).put("amount",holder.binding.price.text)
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })

        holder.binding.offerPrice.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                jsonArray.getJSONObject(position).put("offer_price",holder.binding.offerPrice.text)
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
            }
        })
        holder.binding.delete.setOnClickListener {
            jsonArray.remove(position)
            notifyItemRemoved(position)
        }
        if(jsonArray.length() > 0){
            productUnitFragment.binding.save.visibility = View.VISIBLE
        }
        else{
            productUnitFragment.binding.save.visibility = View.GONE
        }
    }
}