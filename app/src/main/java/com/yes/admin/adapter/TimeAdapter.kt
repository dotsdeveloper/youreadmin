package com.yes.admin.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.recyclerview.widget.RecyclerView
import com.yes.admin.R
import com.yes.admin.activity.CheckoutActivity
import com.yes.admin.databinding.CardTimeBinding
import com.yes.admin.utils.BaseUtils
import com.yes.admin.utils.UiUtils

class TimeAdapter(
    var checkoutActivity: CheckoutActivity,
    var context: Context,
    var list: Array<String>?
) :
    RecyclerView.Adapter<TimeAdapter.HomeHeaderViewHolder>() {
    var selectedPosition = -1
    inner class HomeHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var binding: CardTimeBinding = CardTimeBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeHeaderViewHolder {
        return HomeHeaderViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.card_time,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: HomeHeaderViewHolder, position: Int) {
        if(selectedPosition == position){
            UiUtils.textViewBgTint(holder.binding.time,checkoutActivity.sharedHelper.primarycolor,null)
            holder.binding.time.setTextColor(Color.WHITE)
            holder.binding.root.setOnClickListener {
                checkoutActivity.deliveryslot = ""
                selectedPosition = -1
                notifyDataSetChanged()
            }
        }
        else{
            if(list!![position].isNotEmpty()){
                holder.binding.time.setTextColor(Color.BLACK)
                holder.binding.time.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(Color.WHITE, BlendModeCompat.SRC_ATOP)
                holder.binding.time.text = BaseUtils.getFormattedDate(list!![position],"HH:MM:SS","hh:mm aa")
                holder.binding.root.setOnClickListener {
                    checkoutActivity.deliveryslot = holder.binding.time.text.toString()
                    selectedPosition = position
                    notifyDataSetChanged()
                }
            }
        }
    }
}