package com.yes.admin.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.yes.admin.models.BaseModel
import com.yes.admin.models.CustomerTypeModel

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
open class CustomerTypeResponse : BaseModel() {
    @JsonProperty("data")
    var data: ArrayList<CustomerTypeModel>? = ArrayList()
}