package com.yes.admin.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.yes.admin.models.BaseModel
import com.yes.admin.models.ProductModel

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
open class AddProductResponse : BaseModel() {
    @JsonProperty("data")
    var data: ProductModel? = ProductModel()
}