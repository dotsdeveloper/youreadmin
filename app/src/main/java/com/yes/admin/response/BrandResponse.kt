package com.yes.admin.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.yes.admin.models.BaseModel
import com.yes.admin.models.BrandModel

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
open class BrandResponse : BaseModel() {
    @JsonProperty("data")
    var data: ArrayList<BrandModel>? = ArrayList()
}