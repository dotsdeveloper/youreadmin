package com.yes.admin.response

import com.google.gson.annotations.SerializedName
import com.yes.admin.models.BrandModel
import com.yes.admin.models.CategoryListModel
import com.yes.admin.models.QuantitiesModel
import com.yes.admin.models.ShopperModel
import org.json.JSONObject
import java.io.Serializable

class GetCartResponse : Serializable {

    @SerializedName("error")
    var error: Boolean? = true

    @SerializedName("status")
    var message: String? = null

    @SerializedName("data")
    var getcart_data: GetCartData? = null

}

class GetCartData : Serializable {

    @SerializedName("order_new")
    var ordernew: JSONObject? = JSONObject()

    @SerializedName("order")
    var order: ArrayList<Order>? = null

    @SerializedName("order_amount")
    var order_amount: String? = null

    @SerializedName("gst")
    var gst: String? = null

    @SerializedName("gst_amount")
    var gst_amount: String? = null

    @SerializedName("delivery_charge")
    var delivery_charge: String? = null

    @SerializedName("grand_total")
    var grand_total: String? = null

    @SerializedName("total_save_value")
    var total_save_value: String? = null

}
class Order : Serializable {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("cart_id")
    var cart_id: String? = null

    @SerializedName("customer_id")
    var customer_id: String? = null

    @SerializedName("shopper_id")
    var shopper_id: String? = null

    @SerializedName("product_id")
    var product_id: Int? = null

    @SerializedName("quantity_id")
    var quantity_id: Int? = null

    @SerializedName("quantity")
    var quantity: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("total")
    var total: String? = null

    @SerializedName("save_value")
    var save_value: String? = null

    @SerializedName("total_save_value")
    var total_save_value: String? = null

    @SerializedName("pincode")
    var pincode: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("is_save")
    var is_save: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("product")
    var product: Product? = null

    @SerializedName("quantities")
    var quantities: QuantitiesModel? = null

    @SerializedName("shopper")
    var shopper: ShopperModel? = null

}

class Product : Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("user_id")
    var user_id: Int? = null

    @SerializedName("category_id")
    var category_id: Int? = null

    @SerializedName("sub_category_id")
    var sub_category_id: Int? = null

    @SerializedName("unit_id")
    var unit_id: Int? = null

    @SerializedName("file")
    var file: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("quantity")
    var quantity: String? = null

    @SerializedName("amount")
    var amount: String? = null

    @SerializedName("offer_price")
    var offer_price: String? = null

    @SerializedName("home_display")
    var home_display: String? = null

    @SerializedName("enquiry")
    var enquiry: String? = null

    @SerializedName("stock_available")
    var stock_available: String? = null

    @SerializedName("display")
    var display: String? = null

    @SerializedName("hotsale")
    var hotsale: String? = null

    @SerializedName("available_from_time")
    var available_from_time: String? = null

    @SerializedName("available_to_time")
    var available_to_time: String? = null

    @SerializedName("is_recommend")
    var is_recommend: String? = null

    @SerializedName("low_stock")
    var low_stock: String? = null

    @SerializedName("ordering")
    var ordering: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("draft")
    var draft: String? = null

    @SerializedName("original_files")
    var original_files: Array<String>? = null

    @SerializedName("savedpercentage")
    var savedpercentage: String? = "0"

    @SerializedName("files")
    var files: Array<String>? = null

    @SerializedName("category")
    var category: Category? = Category()

    @SerializedName("brand_id")
    var brand_id: Int = 0

    @SerializedName("brand")
    var brand: BrandModel = BrandModel()
}

class Category : Serializable{
    @SerializedName("name")
    var name: String = ""

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("file")
    var file: String? = ""

    @SerializedName("ccount")
    var ccount: Int? = null
}


class Ordernew : Serializable {

}