package com.yes.youreagency.responses

import com.google.gson.annotations.SerializedName
import com.yes.admin.models.LoginModel

class GetCustomerResponse {
    @SerializedName("error")
    var error: Boolean? = false

    @SerializedName("status")
    var message: String? = ""

    @SerializedName("data")
    var data: ArrayList<LoginModel>? = null
}