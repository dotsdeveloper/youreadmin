package com.yes.admin.response

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.yes.admin.models.AllCountModel
import com.yes.admin.models.BaseModel

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
open class AllCountResponse : BaseModel() {
    @JsonProperty("data")
    var data: AllCountModel? = AllCountModel()
}