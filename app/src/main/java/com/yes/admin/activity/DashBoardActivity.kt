package com.yes.admin.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.messaging.FirebaseMessaging
import com.yes.admin.R
import com.yes.admin.databinding.ActivityMainBinding
import com.yes.admin.interfaces.DialogCallBack
import com.yes.admin.session.Constants
import com.yes.admin.session.TempSingleton
import com.yes.admin.utils.BaseUtils
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils
import java.io.ByteArrayOutputStream
import java.io.File


class DashBoardActivity : BaseActivity() {

    lateinit var navController:NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private var uploadFile: File? = null
    var requestCode = 0
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                // There are no request codes
                val data: Intent? = result.data
                Log.d("ncv", "" + result.resultCode + "cchjbv" + requestCode)
                when (requestCode) {
                    Constants.RequestCode.GALLERY_REQUEST,//gallery
                    -> {
                        DialogUtils.showLoader(this)
                        handleGallery(data)
                    }
                    Constants.RequestCode.CAMERA_REQUEST,//camera
                    -> {
                        DialogUtils.showLoader(this)
                        handleCamera()
                    }
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)
        //supportActionBar!!.setDisplayShowTitleEnabled(false)
        binding.appBarMain.fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
            R.id.nav_home,
            R.id.nav_category,
            R.id.nav_subcategory,
            R.id.nav_products,
            R.id.nav_measurements,
            R.id.nav_brand,
            R.id.nav_pos,
            R.id.nav_orders,
            R.id.nav_profile
        ), drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        load()
    }

    override fun onResume() {
        super.onResume()
        getCustomerType()
        getCategory()
        getSubCategory()
        getBrand()
        getMeasurements()
    }

    private fun logoutDialog(){
        DialogUtils.showAlertDialog(this,getString(R.string.are_you_sure_you_want_to_logout),"",getString(R.string.ok),getString(R.string.cancel), object :
            DialogCallBack {
            override fun onPositiveClick(value: String) {
                sharedHelper.loggedIn = false
                sharedHelper.token = ""
                sharedHelper.id = 0
                sharedHelper.name = ""
                sharedHelper.email = ""
                sharedHelper.mobileNumber = ""
                sharedHelper.countryCode = 0
                BaseUtils.startActivity(this@DashBoardActivity,LoginActivity(),null,true)
            }

            override fun onNegativeClick() {

            }
        })
    }

    fun load(){
        val headerView = binding.navView.getHeaderView(0)
        val navUsername = headerView.findViewById<View>(R.id.name) as TextView
        val navMail = headerView.findViewById<View>(R.id.mail) as TextView
        navUsername.text = sharedHelper.name
        navMail.text = sharedHelper.email
        TempSingleton.clearAllValues()
        updateToken()
        getCustomerType()
        getCategory()
        getSubCategory()
        getBrand()
        getMeasurements()
    }

    private fun updateToken(){
        if(sharedHelper.fcmToken == ""){
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(com.google.firebase.messaging.Constants.MessageNotificationKeys.TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }
                // Get new FCM registration token
                val token = task.result
                // Log and toast
                sharedHelper.fcmToken = token.toString()
                Log.d("FcmToken : ",""+sharedHelper.fcmToken)
                apiConnection.uploadTokenData(this).observe(this) {
                    it?.let {
                        it.error.let { error ->
                            if (error) {
                                //UiUtils.showSnack(binding.root, it.status)
                            }
                        }
                    }
                }
            })
        }
        else{
            apiConnection.uploadTokenData(this).observe(this) {
                it?.let {
                    it.error.let { error ->
                        if (error) {
                           // UiUtils.showSnack(binding.root, it.status)
                        }
                    }
                }
            }
        }
    }

    private fun getCustomerType(){
        DialogUtils.showLoader(this)
        apiConnection.getCUstomerType(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        sharedHelper.customerTypeList = ArrayList()
                    }
                    else {
                        sharedHelper.customerTypeList = it.data!!
                    }
                }
            }
        }
    }

    private fun getCategory(){
        DialogUtils.showLoader(this)
        apiConnection.getCategory(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        sharedHelper.categoryList = ArrayList()
                    }
                    else {
                        sharedHelper.categoryList = it.data!!
                    }
                }
            }
        }
    }

    private fun getSubCategory(){
        DialogUtils.showLoader(this)
        apiConnection.getSubCategory(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        sharedHelper.subcategoryList = ArrayList()
                    }
                    else {
                        sharedHelper.subcategoryList = it.data!!
                    }
                }
            }
        }
    }

    private fun getBrand(){
        DialogUtils.showLoader(this)
        apiConnection.getBrand(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        sharedHelper.brandList = ArrayList()
                    }
                    else {
                        sharedHelper.brandList = it.data!!
                    }
                }
            }
        }

    }

    private fun getMeasurements(){
        DialogUtils.showLoader(this)
        apiConnection.getMeasurement(this).observe(this) {
            DialogUtils.dismissLoader()
            it?.let {
                it.error.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.status)
                        sharedHelper.productUnitList = ArrayList()
                    }
                    else {
                        sharedHelper.productUnitList = it.data!!
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings ->{
                logoutDialog()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun handleGallery(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                uploadFile = File(BaseUtils.getRealPathFromURI(this, uri)!!)
                val filePath = BaseUtils.getRealPathFromUriNew(this, uri)
                if (filePath != null) {
                    Log.d("filePath", java.lang.String.valueOf(filePath))
                    val bitmap: Bitmap = BitmapFactory.decodeFile(filePath)
                    val imagename = System.currentTimeMillis()
                    UiUtils.loadCustomImage(TempSingleton.getInstance().imageview1, BaseUtils.getRealPathFromUriNew(this, uri))
                    TempSingleton.getInstance().imageview2!!.visibility = View.VISIBLE
                    uploadPic(bitmap,"file","$imagename.jpg")
                }
                DialogUtils.dismissLoader()
                // uploadFiles(BaseUtils.getRealPathFromURI(this, uri),"")
            } else {
                // UiUtils.showSnack(binding.root, "null")
                DialogUtils.dismissLoader()
            }
        } else {
            DialogUtils.dismissLoader()
        }
    }

    private fun getFileDataFromDrawable(bitmap: Bitmap): ByteArray? {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }

    private fun handleCamera() {
        uploadFile = File(sharedHelper.imageUploadPath)
        if (uploadFile!!.exists()) {
            val imagename = System.currentTimeMillis()
            val bitmap = BitmapFactory.decodeFile(sharedHelper.imageUploadPath)
            UiUtils.loadCustomImage(TempSingleton.getInstance().imageview1, sharedHelper.imageUploadPath)
            TempSingleton.getInstance().imageview2!!.visibility = View.VISIBLE
            uploadPic(bitmap,"file","$imagename.jpg")
            DialogUtils.dismissLoader()
        } else {
            DialogUtils.dismissLoader()
            UiUtils.showSnack(binding.root, getString(0))
        }
    }
}