package com.yes.admin.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.yes.admin.databinding.ActivitySuccessBinding
import com.yes.admin.utils.UiUtils
import com.yes.admin.viewmodels.MapViewModel


class SuccessActivity : BaseActivity() {
    lateinit var binding: ActivitySuccessBinding
    var mapViewModel: MapViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_success)
        binding = ActivitySuccessBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        UiUtils.textViewTextColor(binding.msgSuccess,sharedHelper.primarycolor,null)
        UiUtils.textViewTextColor(binding.trackorder,sharedHelper.primarycolor,null)
        UiUtils.textViewBgTint(binding.shopmore,sharedHelper.primarycolor,null)
        UiUtils.imageViewTint(binding.img,sharedHelper.primarycolor,null)
        UiUtils.notificationBar(this,sharedHelper.primarycolor,null)

        binding.orderid.text = "Your order number is # "+intent.getStringExtra("orderid").toString()

        binding.shopmore.setOnClickListener {
            finish()
            val intent = Intent(this, DashBoardActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.putExtra("isorder",true)
            startActivity(intent)
        }

        binding.trackorder.setOnClickListener {
            if(intent.getStringExtra("orderid").toString().isNotEmpty()){

            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        val intent = Intent(this, DashBoardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra("isorder",true)
        startActivity(intent)
    }
}