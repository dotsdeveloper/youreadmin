package com.yes.admin.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.yes.admin.databinding.ActivityCustomerBinding
import com.yes.admin.session.SharedHelper
import com.yes.admin.utils.DialogUtils
import com.yes.admin.utils.UiUtils
import com.yes.admin.viewmodels.MapViewModel
import com.yes.admin.adapter.CustomerListAdapter

class CustomerActivity : AppCompatActivity() {
    lateinit var binding: ActivityCustomerBinding
    var mapViewModel: MapViewModel? = null
    var sharedHelper: SharedHelper? = null
    var isselect = false
    var suserid = 0
    var sname = ""
    var smobile = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCustomerBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //setContentView(R.layout.activity_customer)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)

        binding.proceed1.setOnClickListener {
            if(binding.mobile.text.toString().isNotEmpty()){
                DialogUtils.showLoader(this)
                mapViewModel?.getcustomerinfo(this,binding.mobile.text.toString())?.observe(this, Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                                binding.linear1.visibility = View.GONE
                                binding.recycler.visibility = View.GONE
                                binding.proceed3.visibility = View.GONE
                                binding.linear2.visibility = View.VISIBLE
                            }
                            else {
                                if(it.data!!.size > 0){
                                    binding.recycler.visibility = View.VISIBLE
                                    binding.recycler.layoutManager = LinearLayoutManager(
                                        this,
                                        LinearLayoutManager.VERTICAL,
                                        false
                                    )
                                    binding.recycler.adapter = CustomerListAdapter(
                                        this,
                                        this,
                                        it.data!!
                                    )
                                }
                            }
                        }
                    }
                })
            }
            else{
                UiUtils.showSnack(binding.root,"Please Fill Details")
            }
        }

        binding.proceed3.setOnClickListener {
            if(isselect){
                DialogUtils.showLoader(this)
                mapViewModel?.cartmapping(this,smobile,sname)?.observe(this, Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            }
                            else {
                                startActivity(Intent(this, CheckoutActivity::class.java)
                                    .putExtra("uname",sname)
                                    .putExtra("umobile",smobile)
                                    .putExtra("uid",suserid))
                                finish()
                            }
                        }
                    }
                })
            }
            else{
                UiUtils.showSnack(binding.root, "Choose Customer")
            }
        }

        binding.proceed2.setOnClickListener {
            if(binding.mobile1.text.toString().isNotEmpty() && binding.name.text.toString().isNotEmpty()){
                DialogUtils.showLoader(this)
                mapViewModel?.cartmapping(this,binding.mobile1.text.toString(),binding.name.text.toString())?.observe(this, Observer {
                    DialogUtils.dismissLoader()
                    it?.let {
                        it.error?.let { error ->
                            if (error) {
                                UiUtils.showSnack(binding.root, it.message!!)
                            }
                            else {
                                startActivity(Intent(this, CheckoutActivity::class.java)
                                    .putExtra("uname",binding.name.text.toString())
                                    .putExtra("umobile",binding.mobile1.text.toString())
                                    .putExtra("uid",""))
                                finish()
                            }
                        }
                    }
                })
            }
            else{
                UiUtils.showSnack(binding.root,"Please Fill Details")
            }
        }
    }
}