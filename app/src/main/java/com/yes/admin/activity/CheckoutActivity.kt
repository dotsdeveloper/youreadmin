package com.yes.admin.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import java.text.SimpleDateFormat
import java.util.*
import org.json.JSONException
import org.json.JSONObject
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import androidx.activity.result.contract.ActivityResultContracts
import com.android.volley.*
import java.io.*
import com.android.volley.DefaultRetryPolicy
import kotlin.collections.ArrayList
import com.android.volley.AuthFailureError
import com.yes.admin.R
import com.yes.admin.adapter.ListAddressAdapter
import com.yes.admin.adapter.TimeAdapter
import com.yes.admin.databinding.ActivityCheckoutBinding
import com.yes.admin.network.UrlHelper
import com.yes.admin.session.Constants
import com.yes.admin.session.SharedHelper
import com.yes.admin.utils.*
import com.yes.admin.viewmodels.MapViewModel
import com.yes.youreagency.responses.TimeData

class CheckoutActivity : AppCompatActivity(),OnMapReadyCallback{
    lateinit var binding: ActivityCheckoutBinding
    var mapViewModel: MapViewModel? = null
    lateinit var sharedHelper: SharedHelper
    var timedata: ArrayList<TimeData>? = ArrayList()
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    private var myLat: Double? = null
    private var myLng: Double? = null
    var map: GoogleMap? = null
    var flat: Double? = 0.0
    var flng: Double? = 0.0
    var ordertype:Int? = 2
    var couponid:String? = ""
    var deliveryid:String? = ""
    var deliverytype:Int? = 2
    var spincode:String? = ""
    var locationname:String? = ""
    var address:String? = ""
    var landmark:String? = ""
    var description:String? = ""
    var deliverydate:String? = ""
    var deliveryslot:String? = ""
    var paymentid:String? = ""
    var walletamount:String? = ""
    var balanceamount:String? = ""
    var orderamount:Double? = null
    var uploadFile: File? = null
    var imagedata1: VolleyMultipartRequest.DataPart? = null
    var imagedata2: VolleyMultipartRequest.DataPart? = null
    var isattach1:Boolean = false
    var isattach2:Boolean = false
    var isaddaddress:Boolean = false
    var isselectedaddress:Boolean = false
    var backcount = 0
    var suserid = 0
    var sname = ""
    var smobile = ""

    var requestCode = 0
    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            // There are no request codes
            val data: Intent? = result.data
            when (requestCode) {
                Constants.RequestCode.GALLERY_REQUEST//gallery
                -> if (result.resultCode == AppCompatActivity.RESULT_OK) {
                    DialogUtils.showLoader(this)
                    handleGallery(data)
                }
                Constants.RequestCode.CAMERA_REQUEST//camera
                -> if (result.resultCode == AppCompatActivity.RESULT_OK) {
                    DialogUtils.showLoader(this)
                    handleCamera()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        mapViewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        sharedHelper = SharedHelper(this)
        UiUtils.relativeLayoutBgTint(binding.top,sharedHelper.primarycolor,null)
        UiUtils.buttonBgTint(binding.confirm,sharedHelper.primarycolor,null)
        UiUtils.notificationBar(this,sharedHelper.primarycolor,null)
        UiUtils.imageViewTint(binding.imgCoupon,sharedHelper.primarycolor,null)
        UiUtils.textViewBgTint(binding.addressTxt,sharedHelper.primarycolor,null)

        sname = intent.getStringExtra("uname").toString()
        smobile = intent.getStringExtra("umobile").toString()
        suserid = intent.getIntExtra("uid",0)

        getcart()
        loadmap()
        gettimeslot()

        binding.back.setOnClickListener {
            onBackPressed()
        }

        binding.confirm.setOnClickListener {
           proceedclick()
        }

        binding.imgCoupon.setOnClickListener {
            if(binding.coupon.text.isNotEmpty()){
                checkcoupon(spincode!!,binding.coupon.text.toString())
            }
            else{
                UiUtils.showSnack(binding.root,"Please Enter Coupon")
            }
        }

        binding.linearDeliverydate.setOnClickListener {
            val calendar= Calendar.getInstance()
            val year1 = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener
            { _, year, monthOfYear, dayOfMonth ->

                var date: String = dayOfMonth.toString()+"-"+(monthOfYear+1)+"-"+year
                var sday: String = BaseUtils.getFormattedDate(date, "dd-MM-yyyy","EEEE")
                Log.d("fggfcg1",""+date)
                Log.d("fggfcg2",""+BaseUtils.getFormattedDate(date, "dd-MM-yyyy","EEEE dd-MMM-yyyy"))
                binding.ddate.text = BaseUtils.getFormattedDate(date, "dd-MM-yyyy","EEEE dd-MMM-yyyy")
                deliverydate = binding.ddate.text.toString()

                if(timedata!!.isNotEmpty()) {
                    for (items in timedata!!) {
                        if (items.day.equals(sday)) {
                            binding.cardTimeslot.visibility = View.VISIBLE
                            binding.recyclerTime.layoutManager = LinearLayoutManager(
                                this,
                                LinearLayoutManager.HORIZONTAL,
                                false
                            )
                            binding.recyclerTime.adapter = TimeAdapter(
                                this,
                                this,
                                items.timing
                            )
                            break
                        }
                        else{
                            binding.cardTimeslot.visibility = View.GONE
                            deliveryslot = ""
                        }
                    }
                }
                else{
                    binding.cardTimeslot.visibility = View.GONE
                    deliveryslot = ""
                }

            }, year1, month, day)

            datePickerDialog.datePicker.minDate = System.currentTimeMillis()
            datePickerDialog.show()
        }

        binding.cardUploadproduct.setOnClickListener {
            isattach1 = true
            isattach2 = false
            DialogUtils.showPictureDialog(this,null,null)
        }

        binding.addaddress.setOnClickListener {
            binding.addaddress.visibility = View.GONE
            binding.linearListadddress.visibility = View.GONE
            binding.linearAddadddress.visibility = View.VISIBLE
            isaddaddress = true
            isselectedaddress = false
            binding.edtAddressfull.setText("")
            binding.edtAddresslandmark.setText("")
            binding.edtAddressname.setText("")
            binding.edtPinAdd.setText("")

            locateMyLocation()
            deliveryid = "0"
        }

        binding.closeAddress.setOnClickListener {
            binding.addaddress.visibility = View.VISIBLE
            binding.linearListadddress.visibility = View.VISIBLE
            binding.linearAddadddress.visibility = View.GONE
            binding.edtAddressfull.setText("")
            binding.edtAddresslandmark.setText("")
            binding.edtAddressname.setText("")
            binding.edtPinAdd.setText("")
            flat = 0.0
            flng = 0.0
            deliveryid = "0"
            isaddaddress = false
            isselectedaddress = true
            getaddress()
        }

        binding.close1.setOnClickListener {
            binding.relativeImg1.visibility = View.GONE
            imagedata1 = null
        }

        loadpage()
    }

    override fun onBackPressed() {
        if(backcount == 0){
            super.onBackPressed()
        }
        else {
            backcount--
            loadpage()
        }
    }

    fun loadpage(){
        if(backcount == 0){
            binding.cardSummary.visibility = View.VISIBLE
            binding.cardAddcoupon.visibility = View.VISIBLE
            binding.cardDeliverydate.visibility = View.VISIBLE
            binding.cardTimeslot.visibility = View.GONE
            binding.cardUploadproduct.visibility = View.VISIBLE
            binding.relativeImg1.visibility = View.GONE
            binding.cardDeliverynotes.visibility = View.VISIBLE
            binding.cardAddress.visibility = View.GONE
            binding.confirm.visibility = View.VISIBLE
            binding.confirm.text = "PROCEED"
            showdeliverydata()
        }
        else if(backcount == 1){
            if(deliverytype == 1){
                binding.cardSummary.visibility = View.VISIBLE
                binding.cardAddcoupon.visibility = View.GONE
                binding.cardDeliverydate.visibility = View.GONE
                binding.cardTimeslot.visibility = View.GONE
                binding.cardUploadproduct.visibility = View.GONE
                binding.relativeImg1.visibility = View.GONE
                binding.cardDeliverynotes.visibility = View.GONE
                binding.cardAddress.visibility = View.GONE
                binding.confirm.visibility = View.VISIBLE
                binding.confirm.text = "PROCEED"
            }
            else if(deliverytype == 2){
                binding.cardSummary.visibility = View.GONE
                binding.cardAddcoupon.visibility = View.GONE
                binding.cardDeliverydate.visibility = View.GONE
                binding.cardTimeslot.visibility = View.GONE
                binding.cardUploadproduct.visibility = View.GONE
                binding.relativeImg1.visibility = View.GONE
                binding.cardDeliverynotes.visibility = View.GONE
                binding.cardAddress.visibility = View.VISIBLE
                binding.confirm.visibility = View.VISIBLE
                binding.confirm.text = "PROCEED"
                getaddress()
            }
        }
    }
    fun proceedclick(){
        if(backcount == 0){
            if(deliverytype == 0){
                UiUtils.showSnack(binding.root,"Choose Delivery Option")
            }
            else{
                backcount = 1
                loadpage()
            }
        }
        else if(backcount == 1){
            if(deliverytype == 1){
                // pickup
                placeorder()
            }
            else if(deliverytype == 2){
                // home delivery
                    if(isaddaddress){
                        locationname = binding.edtAddressname.text.toString()
                        address = binding.edtAddressfull.text.toString()
                        landmark = binding.edtAddresslandmark.text.toString()
                        spincode = binding.edtPinAdd.text.toString()
                        if(locationname.equals("") || locationname!!.isEmpty()){
                            UiUtils.showSnack(binding.root,"Please Give Address Name")
                        }
                        else if(address.equals("") || address!!.isEmpty()){
                            UiUtils.showSnack(binding.root,"Please Give Address")
                        }
                        else if(landmark.equals("") || landmark!!.isEmpty()){
                            UiUtils.showSnack(binding.root,"Please Give Landmark")
                        }
                        else if(spincode.equals("") || spincode!!.isEmpty()){
                            UiUtils.showSnack(binding.root,"Please Give Pincode")
                        }
                        else{
                            placeorder()
                        }
                    }
                    else if(isselectedaddress){
                        if(deliveryid.equals("0")){
                            UiUtils.showSnack(binding.root,"Choose Address")
                        }
                        else{
                            placeorder()
                        }
                    }
                    else{
                        UiUtils.showSnack(binding.root,"Please select or add address")
                    }
            }
        }
    }

    fun getcart(){
        DialogUtils.showLoader(this)
        mapViewModel?.getcart(this,sharedHelper.cartid_temp)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding.root, it.message!!)
                    }
                    else {
                        orderamount = it.getcart_data!!.grand_total!!.toDouble()
                        if(it.getcart_data!!.total_save_value!!.toInt() != 0){
                            binding.saved.visibility = View.VISIBLE
                            binding.saved.text = "You Saved : "+ Constants.Common.CURRENCY+UiUtils.formattedValues(it.getcart_data!!.total_save_value!!)
                        }
                        else{
                            binding.saved.visibility = View.GONE
                        }
                        binding.total2.text = Constants.Common.CURRENCY+UiUtils.formattedValues(it.getcart_data!!.grand_total!!)
                        if(it.getcart_data!!.order!!.size == 1){
                            binding.subtotalTxt.text = "Subtotal ("+it.getcart_data!!.order!!.size.toString()+" item)"
                        }
                        else{
                            binding.subtotalTxt.text = "Subtotal ("+it.getcart_data!!.order!!.size.toString()+" items)"
                        }
                        binding.subtotal.text = Constants.Common.CURRENCY+UiUtils.formattedValues(it.getcart_data!!.order_amount!!)
                        binding.gstPer.text = "GST ("+it.getcart_data!!.gst.toString()+"%)"
                        binding.gstAmount.text =  Constants.Common.CURRENCY+UiUtils.formattedValues(it.getcart_data!!.gst_amount!!)

                    }
                }
            }
        })
    }

    fun checkcoupon(pincode: String,coupon: String) {
        DialogUtils.showLoader(this)
        mapViewModel?.checkcoupon(this,coupon,pincode)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        UiUtils.showSnack(binding.root, it.message!!)
                    }
                    else {

                        if(it.data != null){
                            binding.imgCoupon.isEnabled = false
                            orderamount = it.data!!.grand_total!!.toDouble()
                            couponid = it.data!!.couppen_id.toString()
                            if(it.data!!.total_save_value!!.toInt() != 0){
                                binding.saved.visibility = View.VISIBLE
                                binding.saved.text = "You Saved : "+ Constants.Common.CURRENCY+UiUtils.formattedValues(it.data!!.total_save_value!!)
                            }
                            else{
                                binding.saved.visibility = View.GONE
                            }
                            binding.total2.text = Constants.Common.CURRENCY+UiUtils.formattedValues(it.data!!.grand_total!!)
                            // binding.subtotalTxt.text = "Subtotal ("+it.data!!.order!!.size.toString()+" items)"
                            binding.subtotal.text = "₹ "+UiUtils.formattedValues(it.data!!.order_amount!!)
                            binding.gstPer.text = "GST ("+it.data!!.gst.toString()+"%)"
                            binding.gstAmount.text =  Constants.Common.CURRENCY+UiUtils.formattedValues(it.data!!.gst_amount!!)
                        }
                        else{
                            UiUtils.showSnack(binding.root, it.message!!)
                        }

                    }
                }
            }
        })
    }
    fun gettimeslot(){
        DialogUtils.showLoader(this)
        mapViewModel?.gettimeslot(this)?.observe(this, Observer {
            DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding.root, it.message!!)
                        timedata = ArrayList()
                      //  showdeliverydata()
                    }
                    else {
                        timedata = it.data
                       // showdeliverydata()
                    }
                }
            }
        })
    }
    fun showdeliverydata() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, 1)
        val tomorrow: Date = calendar.getTime()
        val date: String = SimpleDateFormat("dd-MMM-yyyy").format(tomorrow)
        val sday: String = SimpleDateFormat("EEEE").format(tomorrow)

        binding.ddate.text = sday+" "+date
        deliverydate = binding.ddate.text.toString()

        if(timedata!!.isNotEmpty()) {
            for (items in timedata!!) {
                if (items.day.equals(sday)) {
                    binding.cardTimeslot.visibility = View.VISIBLE
                    binding.recyclerTime.layoutManager = LinearLayoutManager(
                        this,
                        LinearLayoutManager.HORIZONTAL,
                        false
                    )
                    binding.recyclerTime.adapter = TimeAdapter(
                        this,
                        this,
                        items.timing
                    )
                    break
                }
                else{
                    binding.cardTimeslot.visibility = View.GONE
                    deliveryslot = ""
                }
            }
        }
        else{
            binding.cardTimeslot.visibility = View.GONE
            deliveryslot = ""
        }

    }
    fun getaddress(){
       // DialogUtils.showLoader(this)
        mapViewModel?.getaddresslist(this,smobile,sname)?.observe(this, Observer {
            //DialogUtils.dismissLoader()
            it?.let {
                it.error?.let { error ->
                    if (error) {
                        //UiUtils.showSnack(binding.root, it.message!!)
                        binding.linearListadddress.visibility = View.GONE
                    }
                    else {
                       /* var naddressdata: ArrayList<AddressData> = ArrayList()
                        for(items in it.addressdata!!){
                            if(spincode.equals(items.pincode)){
                                naddressdata.add(items)
                            }
                        }*/

                        if(it.addressdata!!.size > 0) {
                            binding.linearListadddress.visibility = View.VISIBLE
                            binding.recyclerAddress.layoutManager = LinearLayoutManager(
                                this,
                                LinearLayoutManager.VERTICAL,
                                false
                            )
                            binding.recyclerAddress.adapter = ListAddressAdapter(
                                1,
                                this,
                                this,
                                it.addressdata!!
                            )
                        }
                        else{
                            binding.linearListadddress.visibility = View.GONE
                        }

                    }
                }
            }
        })
    }

    fun loadmap(){
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationListner()
        askLocationPermission()
    }
    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap) {
        map = p0

        // Add a marker in Sydney and move the camera
       // val sydney = LatLng(23.6611815, 42.9249046)
//        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        // map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 9.0f))

        map?.isMyLocationEnabled = true
        map?.uiSettings!!.isZoomGesturesEnabled = true
        map?.uiSettings!!.isZoomControlsEnabled = true
        map?.isTrafficEnabled = true

        map?.setOnCameraIdleListener {

            var centerLatLng = map?.cameraPosition?.target
            flat = centerLatLng?.latitude
            flng = centerLatLng?.longitude

            val geocoder = Geocoder(this)
            try {
                val addresses = flat?.let {
                    flng?.let { it1 ->
                        geocoder.getFromLocation(
                            it,
                            it1, 1
                        )
                    }
                }
                if (addresses != null && addresses.size > 0) {
                    val address1 = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    val landmark1 = addresses[0].thoroughfare
                    val postalCode = addresses[0].postalCode
                   /* val city = addresses[0].locality
                    val state = addresses[0].adminArea
                    val countrycode = addresses[0].countryCode
                    val country = addresses[0].countryName
                    val knownName = addresses[0].featureName*/

                    binding.edtAddressfull.setText(""+address1)
                    binding.edtPinAdd.setText(""+postalCode)
                    binding.edtAddresslandmark.setText(""+landmark1)

                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == Constants.RequestCode.LOCATION_REQUEST) {
            if (grantResults.size >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation()
            }
        }

    }
    private fun locationListner() {
        locationRequest = LocationRequest.create()
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest?.interval = (20 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
               /* if (p0.locations == null) {
                    return
                }*/
                for (location in p0.locations) {
                    if (location != null) {
                        if (myLat == 0.0 && myLng == 0.0)
                            getLastKnownLocation()
                    }
                }
            }
        }

    }
    private fun askLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    Constants.Permission.LOCATION_LIST,
                    Constants.RequestCode.LOCATION_REQUEST
                )
                return
            } else {
                getUserLocation()
            }
        } else {
            getUserLocation()
        }
    }

    private fun getLastKnownLocation() {

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient?.lastLocation?.addOnSuccessListener { location ->
            // Got last known location. In some rare situations this can be null.

            if(location != null){
                myLat = location.latitude
                myLng = location.longitude

                map?.let {
                    flat = myLat
                    flng = myLng
                    locateMyLocation()
                }
            }
        }

    }
    private fun getUserLocation() {

        GpsUtils(this).turnGPSOn(object : GpsUtils.OnGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                if (isGPSEnable) {
                    getLastKnownLocation()
                }
            }
        })
    }
    private fun locateMyLocation() {
        Log.d("flat",""+flat)
        Log.d("flng",""+flng)
        flat?.let { lat ->
            flng?.let { lng ->
                map?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 12.0f))
            }
        }
    }

  /*  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Constants.RequestCode.GALLERY_INTENT//gallery
            -> if (resultCode == AppCompatActivity.RESULT_OK) {
                DialogUtils.showLoader(this)
                handleGallery(data)
            }
            Constants.RequestCode.CAMERA_INTENT//camera
            -> if (resultCode == AppCompatActivity.RESULT_OK) {
                DialogUtils.showLoader(this)
                handleCamera()
            }
        }
    }*/

    private fun handleGallery(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                uploadFile = File(BaseUtils.getRealPathFromURI(this, uri)!!)

              /*  // convert base64
                var imageStream: InputStream? = null
                try {
                    imageStream = this.contentResolver.openInputStream(uri)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                }
                val yourSelectedImage = BitmapFactory.decodeStream(imageStream)
                Log.d("BASE64",""+ encodeTobase64(yourSelectedImage))
               Log.d("BYTEARRAY",""+ convertToBlob(encodeTobase64(yourSelectedImage)!!))
*/
                var filePath = BaseUtils.getRealPathFromUriNew(this, uri)
                if (filePath != null) {
                    Log.d("filePath", java.lang.String.valueOf(filePath))
                    var bitmap1:Bitmap = BitmapFactory.decodeFile(filePath)
                    val imagename = System.currentTimeMillis()
                    if(isattach1){
                        binding.relativeImg1.visibility = View.VISIBLE
                        UiUtils.loadImage(
                            binding.attachImg1,
                            BaseUtils.getRealPathFromUriNew(this, uri),
                            ContextCompat.getDrawable(this, R.drawable.banner_loader1)!!
                        )
                        imagedata1 = VolleyMultipartRequest.DataPart(
                            "$imagename.jpg",
                            getFileDataFromDrawable(bitmap1)
                        )
                    }
                }
                DialogUtils.dismissLoader()
               // uploadFiles(BaseUtils.getRealPathFromURI(this, uri),"")
            } else {
               // UiUtils.showSnack(binding.root, "null")
                DialogUtils.dismissLoader()
            }
        }
        else{
            DialogUtils.dismissLoader()
        }
    }
    fun getFileDataFromDrawable(bitmap: Bitmap): ByteArray? {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }

    private fun handleCamera() {
        uploadFile = File(sharedHelper.imageUploadPath)
        if (uploadFile!!.exists()) {
            val imagename = System.currentTimeMillis()
            var bitmap1:Bitmap = BitmapFactory.decodeFile(sharedHelper.imageUploadPath)
            if(isattach1){
                binding.relativeImg1.visibility = View.VISIBLE
                UiUtils.loadImage(
                    binding.attachImg1,
                    sharedHelper.imageUploadPath,
                    ContextCompat.getDrawable(this, R.drawable.banner_loader1)!!
                )
                imagedata1 = VolleyMultipartRequest.DataPart(
                    "$imagename.jpg",
                    getFileDataFromDrawable(bitmap1)
                )

            }

         /*   convertToBase64(sharedHelper.imageUploadPath)
            Log.d("BASE64",""+ convertToBase64(sharedHelper.imageUploadPath))
            Log.d("BYTEARRAY",""+ convertToBlob(convertToBase64(sharedHelper.imageUploadPath)!!))
            ffile = convertToBlob(convertToBase64(sharedHelper.imageUploadPath)!!)

            val filePath: String = uploadFile!!.getPath()
            val bitmap = BitmapFactory.decodeFile(filePath)
            ffile1 = bitmap*/
            DialogUtils.dismissLoader()
        } else {
            DialogUtils.dismissLoader()
            UiUtils.showSnack(binding.root, getString(0))
        }
    }


    fun placeorder(){
        description = binding.deliverynotes.text.toString()


        if(deliverytype == 1){
                // pickup
            placeordernew()
        }
        else if(deliverytype == 2){
                // home delivery
                    if(isaddaddress){
                        locationname = binding.edtAddressname.text.toString()
                        address = binding.edtAddressfull.text.toString()
                        landmark = binding.edtAddresslandmark.text.toString()
                        if(locationname.equals("") || locationname!!.isEmpty()){
                            UiUtils.showSnack(binding.root,"Please Give Address Name")
                        }
                        else if(address.equals("") || address!!.isEmpty()){
                            UiUtils.showSnack(binding.root,"Please Give Address")
                        }
                        else if(landmark.equals("") || landmark!!.isEmpty()){
                            UiUtils.showSnack(binding.root,"Please Give Landmark")
                        }
                        else if(spincode.equals("") || spincode!!.isEmpty()){
                            UiUtils.showSnack(binding.root,"Please Give Pincode")
                        }
                        else if(ordertype == 0){
                            UiUtils.showSnack(binding.root,"Choose Payment Mode")
                        }
                        else{
                            placeordernew()
                        }
                    }
                    else if(isselectedaddress){
                        if(deliveryid.equals("0")){
                            UiUtils.showSnack(binding.root,"Choose Address")
                        }
                        else{
                            if(ordertype == 0){
                                UiUtils.showSnack(binding.root,"Choose Payment Mode")
                            }
                            else{
                                placeordernew()
                            }
                        }
                    }
                    else{
                        UiUtils.showSnack(binding.root,"Please select or add address")
                    }
            }

    }

    private fun placeordernew() {
        DialogUtils.showLoader(this)
        val volleyMultipartRequest: VolleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, UrlHelper.PLACEORDER,
            Response.Listener<NetworkResponse?> { response ->
                DialogUtils.dismissLoader()
                Log.d("orderresponse2", "" + String(response.data))
                try {
                    val obj = JSONObject(String(response.data))
                    if (!obj.getBoolean("error")) {
                        finish()
                        startActivity(Intent(this, SuccessActivity::class.java)
                            .putExtra("orderid",obj.getString("order_id")))
                    }
                    else {
                        UiUtils.showSnack(binding.root, obj.getString("status"))
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    UiUtils.showSnack(binding.root,"Backend Side Issue")
                }
            },
            Response.ErrorListener { error ->
                DialogUtils.dismissLoader()
                Log.d("hfhg",""+error)
                Log.d("hfhg2",""+error.message)
                if(error.networkResponse != null){
                    val response: NetworkResponse = error.networkResponse
                    Log.d("hgvxdhf3",""+String(response.data))
                }

            })
        {
            override fun getHeaders(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["Authorization"] = "Bearer " +sharedHelper.token
                Log.d("param header", "" + params)
                return params
            }

            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params: MutableMap<String, String> = HashMap()
                params["shopper_id"] = sharedHelper.branchid.toString()
                params["order_type"] = ordertype.toString()
                params["coupon_id"] = couponid.toString()
                params["delivery_type"] = deliverytype.toString()
                params["pincode"] = spincode.toString()
                params["location_name"] = locationname.toString()
                params["address"] = address.toString()
                params["land_mark"] = landmark.toString()
                if(isselectedaddress){
                    params["location_lat"] = flat.toString()
                    params["location_long"] = flng.toString()
                    params["delivery_id"] = deliveryid.toString()
                }
                else if(isaddaddress){
                    params["location_lat"] = flat.toString()
                    params["location_long"] = flng.toString()
                    params["delivery_id"] = ""
                }
                else{
                    params["location_lat"] = ""
                    params["location_long"] = ""
                    params["delivery_id"] = ""
                }
                params["description"] = description.toString()
                params["delivery_date"] = deliverydate.toString()
                params["delivery_slot"] = deliveryslot.toString()
                params["payment_id"] = paymentid.toString()
                params["wallet_amount"] = walletamount.toString()
                params["balance_amount"] = balanceamount.toString()
                params["manual"] = "0"
                params["name"] = sname
                params["mobile"] = smobile
                Log.d("param order body", "" + params)
                return params
            }

            override fun getByteData(): Map<String, DataPart> {
                val params: MutableMap<String, DataPart> = HashMap()

                // multi files
                /*val uploads = arrayOfNulls<File>(images.size())
                for (i in 0 until images.size()) {
                    try {
                        params["uploads[$i]"] = DataPart(FileName + i + ".jpg", b, "image/*")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }*/*/

                if(imagedata1 != null && imagedata2 != null) {
                    // long imagename = System.currentTimeMillis();
                    params["file"] = imagedata2!!
                    params["non_image"] = imagedata1!!
                }
                else if(imagedata1 != null){
                    params["non_image"] = imagedata1!!
                }
                else if(imagedata2 != null){
                    params["file"] = imagedata2!!
                }
                Log.d("param img", "" + params)
                return params
            }
        }

        //adding the request to volley
        //Volley.newRequestQueue(this).add(volleyMultipartRequest)
        VolleySingleton.getInstance(this).addToRequestQueue(volleyMultipartRequest.setRetryPolicy(DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)))
    }
}