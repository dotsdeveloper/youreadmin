package com.yes.admin.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.yes.admin.network.Api
import com.yes.admin.network.ApiInput
import com.google.gson.Gson
import com.yes.admin.response.GetCartResponse
import com.yes.admin.interfaces.ApiResponseCallback
import com.yes.admin.response.AddCartResponse
import com.yes.admin.response.ProductListResponse
import com.yes.admin.utils.BaseUtils
import com.yes.youreagency.responses.*
import org.json.JSONArray
import org.json.JSONObject

class MapRepository private constructor() {

    companion object {
        var repository: MapRepository? = null

        fun getInstance(): MapRepository {
            if (repository == null) {
                repository = MapRepository()
            }
            return repository as MapRepository
        }
    }

    fun addtocart(input: ApiInput): LiveData<AddCartResponse>? {
        val apiResponse: MutableLiveData<AddCartResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddCartResponse =
                    gson.fromJson(jsonObject.toString(), AddCartResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddCartResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getcart(input: ApiInput): LiveData<GetCartResponse>? {
        val apiResponse: MutableLiveData<GetCartResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetCartResponse =
                    gson.fromJson(jsonObject.toString(), GetCartResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetCartResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun deletecart(input: ApiInput): LiveData<AddCartResponse>? {
        val apiResponse: MutableLiveData<AddCartResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: AddCartResponse =
                    gson.fromJson(jsonObject.toString(), AddCartResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = AddCartResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getallproduct(input: ApiInput): LiveData<ProductListResponse>? {
        val apiResponse: MutableLiveData<ProductListResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                if(jsonObject.getBoolean("error")){
                    val response: ProductListResponse =
                        gson.fromJson(jsonObject.toString(), ProductListResponse::class.java)
                    apiResponse.value = response
                }
                else{
                    jsonObject.put("data",productScan(jsonObject.getJSONArray("data")))
                    val response: ProductListResponse =
                        gson.fromJson(jsonObject.toString(), ProductListResponse::class.java)
                    apiResponse.value = response
                }
            }

            override fun setErrorResponse(error: String) {
                var response = ProductListResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun productScan(jsonArray: JSONArray): JSONArray {
        for (i in 0 until jsonArray.length()) {
            if(jsonArray.getJSONObject(i).has("files")){
                val files = jsonArray.getJSONObject(i).get("files")
                if(!BaseUtils.isArray(files)){
                    jsonArray.getJSONObject(i).put("files", JSONArray())
                }
            }
            if(jsonArray.getJSONObject(i).has("original_files")){
                val oFiles = jsonArray.getJSONObject(i).get("original_files")
                if(!BaseUtils.isArray(oFiles)){
                    jsonArray.getJSONObject(i).put("original_files", JSONArray())
                }
            }
        }
        return jsonArray
    }
    fun checkcoupon(input: ApiInput): LiveData<CheckCouponResponse>? {
        val apiResponse: MutableLiveData<CheckCouponResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CheckCouponResponse =
                    gson.fromJson(jsonObject.toString(), CheckCouponResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CheckCouponResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getaddresslist(input: ApiInput): LiveData<GetLocationResponse>? {
        val apiResponse: MutableLiveData<GetLocationResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetLocationResponse =
                    gson.fromJson(jsonObject.toString(), GetLocationResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetLocationResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }


    fun addaddress(input: ApiInput): LiveData<CommonResponse>? {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun gettimeslot(input: ApiInput): LiveData<TimeslotResponse>? {
        val apiResponse: MutableLiveData<TimeslotResponse> = MutableLiveData()
        Api.getMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: TimeslotResponse =
                    gson.fromJson(jsonObject.toString(), TimeslotResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = TimeslotResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun getcustomerinfo(input: ApiInput): LiveData<GetCustomerResponse>? {
        val apiResponse: MutableLiveData<GetCustomerResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: GetCustomerResponse =
                    gson.fromJson(jsonObject.toString(), GetCustomerResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = GetCustomerResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }

    fun cartmapping(input: ApiInput): LiveData<CommonResponse>? {
        val apiResponse: MutableLiveData<CommonResponse> = MutableLiveData()
        Api.postMethod(input, object : ApiResponseCallback {
            override fun setResponseSuccess(jsonObject: JSONObject) {
                val gson = Gson()
                var response: CommonResponse =
                    gson.fromJson(jsonObject.toString(), CommonResponse::class.java)
                apiResponse.value = response
            }

            override fun setErrorResponse(error: String) {
                var response = CommonResponse()
                response.error = true
                apiResponse.value = response
            }
        })

        return apiResponse
    }
}