package com.yes.admin.interfaces

interface AnimationCallBack {
    fun onAnimationStart()
    fun onAnimationEnd()
    fun onAnimationRepeat()
}