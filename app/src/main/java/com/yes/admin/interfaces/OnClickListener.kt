package com.yes.admin.interfaces

import com.yes.admin.models.BrandModel
import com.yes.admin.models.CategoryListModel
import com.yes.admin.models.ProductUnitModel
import com.yes.admin.models.SubCategoryListModel

interface OnClickListener {
    fun onClickItem(data1: CategoryListModel,data2: SubCategoryListModel,data3: BrandModel,data4: ProductUnitModel)
    fun onClickItem()
}