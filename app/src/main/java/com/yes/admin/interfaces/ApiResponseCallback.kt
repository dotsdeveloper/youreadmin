package com.yes.admin.interfaces

import org.json.JSONObject

interface ApiResponseCallback {
    fun setResponseSuccess(jsonObject: JSONObject)
    fun setErrorResponse(error : String)
}