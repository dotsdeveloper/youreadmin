package com.yes.admin.interfaces

interface DialogCallBack {
    fun onPositiveClick(value : String)
    fun onNegativeClick()
}