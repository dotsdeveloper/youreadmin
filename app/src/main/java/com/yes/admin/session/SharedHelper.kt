package com.yes.admin.session

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.yes.admin.models.*
import com.yes.admin.response.Order

class SharedHelper(context: Context) {

    private var sharedPreference: SharedPref = SharedPref(context)

    var isInstallFirst: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean(Constants.SessionKeys.IS_INSTALL_FIRST)
        }
        set(value) {
            sharedPreference.putBoolean(Constants.SessionKeys.IS_INSTALL_FIRST, value)
        }

    var token: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.TOKEN)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.TOKEN, value)
        }

    var id: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.ID)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.ID, value)
        }

    var shop_id: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.SHOP_ID)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.SHOP_ID, value)
        }

    var fcmToken: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.FCM_TOKEN)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.FCM_TOKEN, value)
        }

    var language: String
        get() : String {
            return if (sharedPreference.getKey(Constants.SessionKeys.LANGUAGE) == "") {
                "en"
            } else {
                sharedPreference.getKey(Constants.SessionKeys.LANGUAGE)
            }

        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.LANGUAGE, value)
        }

    var name: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.NAME)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.NAME, value)
        }

    var email: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.EMAIL)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.EMAIL, value)
        }

    var mobileNumber: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.MOBILE_NUMBER)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.MOBILE_NUMBER, value)
        }

    var countryCode: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.COUNTRY_CODE)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.COUNTRY_CODE, value)
        }

    var loggedIn: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean(Constants.SessionKeys.LOGGED_IN)
        }
        set(value) {
            sharedPreference.putBoolean(Constants.SessionKeys.LOGGED_IN, value)
        }

    var location1: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.LOCATION1)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.LOCATION1, value)
        }

    var myLat: Float
        get() : Float {
            return sharedPreference.getFloat(Constants.SessionKeys.LAT)
        }
        set(value) {
            sharedPreference.putFloat(Constants.SessionKeys.LAT, value)
        }

    var myLng: Float
        get() : Float {
            return sharedPreference.getFloat(Constants.SessionKeys.LNG)
        }
        set(value) {
            sharedPreference.putFloat(Constants.SessionKeys.LNG, value)
        }

    var imageUploadPath: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.IMAGE_UPLOAD_PATH)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.IMAGE_UPLOAD_PATH, value)
        }

    var notifyCount: String
        get() : String {
            return sharedPreference.getKey(Constants.SessionKeys.NOTIFY_COUNT)
        }
        set(value) {
            sharedPreference.putKey(Constants.SessionKeys.NOTIFY_COUNT, value)
        }

    var pOrderCount: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.P_ORDER_COUNT)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.P_ORDER_COUNT, value)
        }

    var cOrderCount: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.C_ORDER_COUNT)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.C_ORDER_COUNT, value)
        }

    var tOrderCount: Int
        get() : Int {
            return sharedPreference.getInt(Constants.SessionKeys.T_ORDER_COUNT)
        }
        set(value) {
            sharedPreference.putInt(Constants.SessionKeys.T_ORDER_COUNT, value)
        }

    var customerTypeList: ArrayList<CustomerTypeModel>
        get() : ArrayList<CustomerTypeModel> {
            val myType = object : TypeToken<List<CustomerTypeModel>>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.CUSTOMER_TYPE_LIST)

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<CustomerTypeModel> = Gson().fromJson(vsl, myType)
            return logs as ArrayList<CustomerTypeModel>
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.CUSTOMER_TYPE_LIST, jsonString)
        }

    var categoryList: ArrayList<CategoryListModel>
        get() : ArrayList<CategoryListModel> {
            val myType = object : TypeToken<List<CategoryListModel>>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.CATEGORY_LIST)

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<CategoryListModel> = Gson().fromJson(vsl, myType)
            return logs as ArrayList<CategoryListModel>
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.CATEGORY_LIST, jsonString)
        }

    var subcategoryList: ArrayList<SubCategoryListModel>
        get() : ArrayList<SubCategoryListModel> {
            val myType = object : TypeToken<List<SubCategoryListModel>>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.SUB_CATEGORY_LIST)

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<SubCategoryListModel> = Gson().fromJson(vsl, myType)
            return logs as ArrayList<SubCategoryListModel>
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.SUB_CATEGORY_LIST, jsonString)
        }

    var brandList: ArrayList<BrandModel>
        get() : ArrayList<BrandModel> {
            val myType = object : TypeToken<List<BrandModel>>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.BRAND_LIST)

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<BrandModel> = Gson().fromJson(vsl, myType)
            return logs as ArrayList<BrandModel>
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.BRAND_LIST, jsonString)
        }

    var productUnitList: ArrayList<ProductUnitModel>
        get() : ArrayList<ProductUnitModel> {
            val myType = object : TypeToken<List<ProductUnitModel>>() {}.type
            val vsl = sharedPreference.getKey(Constants.SessionKeys.PRODUCT_UNIT_LIST)

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<ProductUnitModel> = Gson().fromJson(vsl, myType)
            return logs as ArrayList<ProductUnitModel>
        }
        set(value) {
            val jsonString = Gson().toJson(value)
            sharedPreference.putKey(Constants.SessionKeys.PRODUCT_UNIT_LIST, jsonString)
        }

    var cartid_temp: String
        get() : String {
            return sharedPreference.getKey("cartid_temp","")
        }
        set(value) {
            sharedPreference.putKey("cartid_temp", value)
        }

    var iscart_temp: Boolean
        get() : Boolean {
            return sharedPreference.getBoolean("iscart_temp")
        }
        set(value) {
            sharedPreference.putBoolean("iscart_temp", value)
        }

    var cartlist: ArrayList<Order>
        get() : ArrayList<Order> {
            val myType = object : TypeToken<List<Order>>() {}.type
            val vsl = sharedPreference.getKey("cartlist")

            if (vsl == "") {
                return ArrayList()
            }
            val logs: List<Order> = Gson().fromJson<List<Order>>(vsl, myType)
            return logs as ArrayList<Order>
        }
        set(value) {
            var jsonString = Gson().toJson(value)
            sharedPreference.putKey("cartlist", jsonString)
        }

    var max_quantity: Int
        get() : Int {
            return sharedPreference.getInt("max_quantity")
        }
        set(value) {
            sharedPreference.putInt("max_quantity", value)
        }


    var stock_option: Int
        get() : Int {
            return sharedPreference.getInt("stock_option")
        }
        set(value) {
            sharedPreference.putInt("stock_option", value)
        }

    var primarycolor: String
        get() : String {
            return sharedPreference.getKey("primarycolor","#067D43")
        }
        set(value) {
            sharedPreference.putKey("primarycolor", value)
        }

    var branchid: Int
        get() : Int {
            return sharedPreference.getInt("branchid")
        }
        set(value) {
            sharedPreference.putInt("branchid", value)
        }

}
