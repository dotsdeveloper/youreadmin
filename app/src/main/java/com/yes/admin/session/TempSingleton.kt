package com.yes.admin.session

import android.widget.ImageView
import okhttp3.MultipartBody
import org.json.JSONArray

class TempSingleton private constructor() {
    companion object {

        private var instane: TempSingleton? = null

        fun getInstance(): TempSingleton {
            if (instane == null) {
                instane = TempSingleton()
            }
            return instane!!
        }

        fun clearAllValues() {
            instane = TempSingleton()
        }
    }

    var mobile: String? = null
    var passsword: String? = null

    var imageview1: ImageView? = null
    var imageview2: ImageView? = null
    var multipartFileBody: MultipartBody.Part? = null
    var isFile:Int = 0

    var pname: String = ""
    var cid: Int = 0
    var scid: Int = 0
    var bid: Int = 0
    var desc: String = ""
    var available_from_time: String = ""
    var available_to_time: String = ""
    var is_recommend: Int = 0
    var home_display: Int = 0
    var lowstock: Int = 0
    var hotsale: Int = 0
    var enquiry: Int = 0
    var file_name: String = ""
    var quantities: JSONArray = JSONArray()
    var files: String = ""


}