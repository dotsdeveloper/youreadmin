package com.yes.admin.helpers

import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import com.yes.admin.R
import com.yes.admin.activity.BaseActivity

class AlertDialogHelper {

    companion object {

        fun showNewCustomDialog(context: BaseActivity?, title: String?, content: String?, cancelable: Boolean = true, positiveButtonText: String? = null, positiveButtonClickListener: DialogInterface.OnClickListener? = null, negativeButtonText: String? = null, negativeButtonClickListener: DialogInterface.OnClickListener? = null) {
            if (context != null && title != null && content != null) {
                if (context.mCustomDialog == null || !context.mCustomDialog!!.isShowing) {
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle(title)
                    builder.setMessage(content)
                    builder.setCancelable(cancelable)
                    if (positiveButtonText != null && positiveButtonClickListener != null) {
                        builder.setPositiveButton(positiveButtonText, positiveButtonClickListener)
                    } else {
                        builder.setPositiveButton(context.resources.getString(R.string.app_name)) { dialogInterface: DialogInterface, _: Int ->
                            dialogInterface.dismiss()
                        }
                    }
                    if (negativeButtonText != null && negativeButtonClickListener != null) {
                        builder.setNegativeButton(negativeButtonText, negativeButtonClickListener)
                    }
                    context.mCustomDialog = builder.create()
                    context.mCustomDialog?.show()
                }
            }
        }

        private fun dismissCustomDialog(context: BaseActivity) {
            if (context.mCustomDialog != null && context.mCustomDialog!!.isShowing) {
                context.mCustomDialog?.dismiss()
            }
        }
    }
}