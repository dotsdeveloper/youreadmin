package com.yes.admin.helpers

import com.yes.admin.R

object ConstantsHelper {

    const val MOBIKUL_CATALOG_HOME_PAGE_DATA = "mobikulhttp/catalog/homepagedata"
    const val MOBIKUL_CUSTOMER_WEB_LOGIN = "login"
    const val GET_ORDER = "delivery_list"
    const val GET_NOTIFY = ApplicationConstants.BASE_URL+"notification/"
    const val GET_PROFILE = "profile"
    const val FORGET_PASSWORD = "password/create"
    const val VERIFY_OTP = "password/verify"
    const val RESET_PASSWORD = "password/reset"
    const val RESEND_OTP = "password/resend"
    const val UPDATEDEVICETOKEN = "api/device"
    const val COMPLETE_DELIVERY = "delivery_status"

    const val GET_CUSTOMER_TYPE = "customer_type"
    const val GET_CATEGORY = "category"
    const val ADD_CATEGORY = "category/store"
    const val UPDATE_CATEGORY = "category/store"
    const val UPDATE_CATEGORY_STATUS = "category/status"
    const val DELETE_CATEGORY = "category/delete"

    const val GET_SUB_CATEGORY = "sub_category"
    const val ADD_SUB_CATEGORY = "sub_category/store"
    const val UPDATE_SUB_CATEGORY = "sub_category/store"
    const val UPDATE_SUB_CATEGORY_STATUS = "sub_category/status"
    const val DELETE_SUB_CATEGORY = "sub_category/delete"

    const val GET_BRAND = "brand"
    const val ADD_BRAND = "brand/store"
    const val UPDATE_BRAND = "brand/store"
    const val UPDATE_BRAND_STATUS = "brand/status"
    const val DELETE_BRAND = "brand/delete"

    const val GET_MEASUREMENT = "product_unit"
    const val ADD_MEASUREMENT = "product_unit/store"
    const val UPDATE_MEASUREMENT = "product_unit/store"
    const val UPDATE_MEASUREMENT_STATUS = "product_unit/status"
    const val DELETE_MEASUREMENT = "product_unit/delete"

    const val GET_PRODUCTS = "product"
    const val ADD_PRODUCT = "product/store"
    const val UPDATE_PRODUCT = "product/store"
    const val UPDATE_PRODUCT_STATUS = "product/status"
    const val DELETE_PRODUCT = "product/delete"

    const val GET_ORDERS = "shopper/order"
    const val GET_CUSTOMERS = "customer"
    const val ADD_CUSTOMER = "customer/add"
    const val GET_ALL_COUNTS = "all_counts"

    const val RC_LOGIN = 1001
    const val RC_SIGN_UP = 1002
    const val RC_WRITE_TO_EXTERNAL_STORAGE = 1003
    const val RC_PICK_IMAGE = 1004
    const val RC_VOICE = 1005
    const val RC_CAMERA_SEARCH = 1006
    const val RC_PICK_SINGLE_FILE = 1007
    const val RC_ADD_EDIT_ADDRESS = 1008
    const val RC_PAYMENT = 1009
    const val RC_GOOGLE_SIGN_IN = 1010
    const val RC_AR = 1011
    const val RC_LOCATION_PERMISSION = 1012
    const val RC_CHECK_LOCATION_SETTINGS = 1013
    const val RC_UPDATE_APP_FROM_PLAY_STORE = 1014
    const val RC_PLACE_PICKER = 1014
    const val RC_QR_LOGIN = 1015
    const val RC_OPEN_SCANNER = 1022
    const val RC_AR_MEASURE = 1016
    const val APP_UPDATE_REQUEST_CODE = 1017
    const val RC_WISHLIST = 1018

    const val CUSTOMER_NOT_EXIST = "customerNotExist"
    const val ORDER_STATUS_COMPLETE = "complete"
    const val ORDER_STATUS_PENDING = "pending"
    const val ORDER_STATUS_PROCESSING = "processing"
    const val ORDER_STATUS_HOLD = "holded"
    const val ORDER_STATUS_CANCELLED = "canceled"
    const val ORDER_STATUS_NEW = "new"
    const val ORDER_STATUS_CLOSED = "closed"
    const val VIEW_TYPE_LIST = 1
    const val VIEW_TYPE_GRID = 2
    const val OPEN_LOGIN_PAGE = 1
    const val OPEN_SIGN_UP_PAGE = 2
    const val DEFAULT_DATE_FORMAT = "MM/dd/yy"

    val GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/"
    /*val GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?"
    val GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?"
    val GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?"
    val GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?"*/
    private val GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL +"geocode/json?"
    fun getAddress(latitude: Double, longitude: Double): String {
        val lat = latitude.toString()
        val lng = longitude.toString()
        return (GOOGLE_API_GEOCODE_BASE_URL + "latlng=" + lat + ","
                + lng + "&sensor=true&key=" + AppController.getInstance().resources.getString(
            R.string.map_api_key
        ))
    }

}